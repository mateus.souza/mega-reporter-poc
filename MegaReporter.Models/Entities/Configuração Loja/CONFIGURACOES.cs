﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Entities.Configurações_Loja
{
    [Table("CONFIGURACOES")]
    public class CONFIGURACOES
    {
        [Key]
        [Column("MEDICAMENTO")]
        public string Medicamento { get; set; } = null!;
        [Column("COMBUSTIVEL")]
        public string Combustivel { get; set; } = null!;
    }
}
