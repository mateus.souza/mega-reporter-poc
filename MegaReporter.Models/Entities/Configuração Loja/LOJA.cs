﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Entities.Loja
{
    [Table("LOJA")]
    public class LOJA
    {
        [Key]
        [Column("ID_LOJA")]
        public int IdLoja { get; set; }

        [Column("NOME_FANTASIA")]
        public string NomeFantasia { get; set; } = null!;
        [Column("SLOGAN")]
        public string Slogan { get; set; } = null!;

        [Column("CNPJ")]
        public string Cnpj { get; set; } = null!;
    }
}
