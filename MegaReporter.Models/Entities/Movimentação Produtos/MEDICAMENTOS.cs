﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Entities
{
    public class MEDICAMENTOS
    {
        [Column("DATA_VENDA")]
        public DateTime DataVenda { get; set; }

        [Column("NOTA_FISCAL")]
        public string? NotaFiscal { get; set; }

        [Key]
        public string? Codigo { get; set; }

        public string? Descricao { get; set; }

        public string? Cfop { get; set; }

        [Column("QUANTIDADE_VENDIDA")]
        public Double QuantidadeVendida { get; set; }

        public string? Lote { get; set; }

        public string? Fabricacao { get; set; }

        public string? Vencimento { get; set; }

        [Column("PRECO_VENDA")]
        public Double PrecoVenda { get; set; }

        public Double Desconto { get; set; }

        [Column("DESCRICAO_PLANO")]
        public string? DescricaoPlano { get; set; }

        public string? Cliente { get; set; }

        [Column("NOME")]
        public string? NomeVendedor { get; set; }

        public string? Marca { get; set; }

        public string? Grupo { get; set; }

        [Column("RAZAO_SOC")]
        public string? RazaoSocial { get; set; }

        [Column("TIPO_SAIDA")]
        public string? TipoSaida { get; set; }

        [Column("PRECO_CUSTO")]
        public Double PrecoCusto { get; set; }

        [Column("PRECO_VENDA_BRUTO")]
        public Double PrecoBruto { get; set; }

        public string? Referencia { get; set; }

        [Column("N_SUB_TOTAL")]
        public Double TotalLiquido { get; set; }

        public string? Operacao { get; set; }

    }
}
