﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MegaReporter.Models.Entities
{
    [Table("SAIDAS")]
    public class PRODUTOS
    {
        [Key]
        public int Id { get; set; }

        [Column("CODIGO")]
        public string Codigo { get; set; } = null!;

        [Column("DATA_VENDA")]
        public DateTime? DataVenda { get; set; }

        public string? Cliente { get; set; } = null!;

        public string? Descricao { get; set; } = null!;


        public string? Cst { get; set; } = null!;

        [Column("Nome")]
        public string? NomeVendedor { get; set; } = null!;

        public string? Ncm { get; set; } = null!;

        public string? Unidade { get; set; } = null!;

        [Column("DESCRICAO_PLANO")]
        public string? DescricaoPlano { get; set; } = null!;

        [Column("QUANTIDADE_VENDIDA")]
        public Double? QuantidadeVendida { get; set; }

        [Column("PRECO_VENDA")]
        public Double? PrecoVenda { get; set; }

        [Column("TIPO_SAIDA")]
        public string? TipoSaida { get; set; } = null!;

        public string? Cfop { get; set; } = null!;


        [Column("base_calculo")]
        public Double? BaseCalculo { get; set; }

        [Column("N_SUB_TOTAL")]
        public Double? TotalLiquido { get; set; }

        [Column("N_SUB_TOTAL_BRUTO")]
        public Double? TotalBruto { get; set; }

        [Column("N_DESCONTO")]
        public Double TotalDesconto { get; set; }

        [Column("ICMS_CREDITO")]
        public Double? IcmsCredito { get; set; }

        [Column("PRECO_VENDA_BRUTO")]
        public Double? PrecoBruto { get; set; }

        public string? Referencia { get; set; }

        public string? Marca { get; set; }

        public string? Grupo { get; set; }

        [Column("RAZAO_SOC")]
        public string? RazaoSocial { get; set; }

        public string? Operacao { get; set; }


        [NotMapped]
        public Double? ValorBaseIcms { get { return ((QuantidadeVendida * PrecoVenda) * BaseCalculo) / 100; } }

        [NotMapped]
        public Double? ValorAliquota { get { return ((((QuantidadeVendida * PrecoVenda) * BaseCalculo) / 100)
                    * IcmsCredito) / 100; } }

    }
}
