﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MegaReporter.Models.Entities
{
    public class COMBUSTIVEIS
    {
        [Key]
        [Column("DATA_EMISSAO")]
        public DateTime DataEmissao { get; set; }

        public string? Cliente { get; set; }
        public string? Descricao { get; set; }
        public Double Quantidade { get; set; }

        [Column("PRECO_VENDA")]
        public Double PrecoVenda { get; set; }

        [Column("DESCRICAO_PLANO")]
        public string? DescricaoPlano { get; set; }

        [Column("NOME")]
        public string? NomeVendedor { get; set; }

        public string? Unidade { get; set; }

        [Column("DESCONTO_VAL_SUBTOTAL")]
        public Double DescontoSubtotal { get; set; }

        public string? Bico { get; set; }

        public string? Bomba { get; set; }

        public string? Tanque { get; set; }

        [Column("TRN_DESCRICAO")]
        public string? DescricaoTurno { get; set; }

        [Column("VEICULO_PLACA")]
        public string? VeiculoPlaca { get; set; }

        [Column("VEICULO_HODOMETRO")]
        public string? VeiculoHodometro { get; set; }

        public string? Vinculada{ get; set; }

        [Column("DATA_VINCULAMENTO")]
        public string? DataVinculamento { get; set; }

        public string? Numero { get; set; }

        public string? Hora { get; set; }
    }
}
