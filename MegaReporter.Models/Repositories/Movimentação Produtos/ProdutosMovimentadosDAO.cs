﻿using MegaReporter.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Immutable;

namespace MegaReporter.Models.Repositories
{
    public class ProdutosMovimentadosDAO : IProdutosMovimentadosDAO<PRODUTOS>
    {
        private readonly MegaReporterDbContext _dbContext;

        public ProdutosMovimentadosDAO(MegaReporterDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public ImmutableList<PRODUTOS> ExibeTodos(string dataInicial, string dataFinal)
        {
            var sql = @$"
                 SELECT
S.ID_SAIDA AS ID,
S.CODIGO,
S.data_venda,
C.cliente,
P.descricao,
S.cfop,
S.quantidade_vendida,
P.cst,
PV.descricao as DESCRICAO_PLANO,
V.nome,
P.unidade,
S.preco_venda,
P.ncm,
S.PRECO_VENDA_BRUTO,
P.GRUPO,
p.REFERENCIA,
S.base_calculo,
COALESCE(S.N_SUB_TOTAL, 0) AS N_SUB_TOTAL,
S.ICMS_CREDITO,
COALESCE(S.N_SUB_TOTAL_BRUTO, 0) AS N_SUB_TOTAL_BRUTO,
COALESCE(S.N_DESCONTO, 0) AS N_DESCONTO,
M.marca,
F.RAZAO_SOC,
S.OPERACAO,
'NFE' AS TIPO_SAIDA
FROM SAIDAS S INNER JOIN produtos P ON S.codigo = P.codigo
INNER JOIN plano_venda PV ON S.id_plano = PV.id_plano
INNER JOIN cliente C ON C.id_cliente = S.id_cliente
INNER JOIN VENDEDOR V ON V.id_vendedor = S.id_vendedor
LEFT JOIN MARCA M ON M.id_marca = P.ID_MARCA
INNER JOIN FORNECEDOR F ON F.ID_FORNECEDOR = S.ID_FORNECEDOR
WHERE S.data_venda between '{dataInicial}' AND '{dataFinal}'
AND S.operacao = 'V'
AND S.quantidade_vendida > 0 AND S.NUMERO_CUPOM=''
and P.DESCRICAO != ''

UNION ALL

SELECT
I.ID_ITEM_NFCE AS ID,
I.codigo,
N.data_emissao,
C.cliente,
P.descricao,
I.cfop,
I.quantidade,
P.cst,
PV.descricao AS DESCRICAO_PLANO ,
V.nome ,
I.unidade,
I.preco_liquido,
P.ncm,
I.PRECO_VENDA ,
P.grupo,
P.referencia ,
I.icms_perc_bc ,
I.total_nf,
I.ICMS_ALIQUOTA,
i.TOTAL_PRODUTOS,
(I.DESCONTO_VAL_ITEM + I.DESCONTO_VAL_SUBTOTAL) AS TOTAL_DESCONTO  ,
M.marca ,
F.razao_soc,
'V' as OPERACAO,
'NFCE' AS TIPO_SAIDA
FROM item_nfce I INNER JOIN NFCE N ON N.id_nfce = I.id_nfce
INNER JOIN CLIENTE C ON C.id_cliente = N.id_cliente
INNER join produtos P ON P.codigo = I.codigo
INNER JOIN plano_venda PV ON PV.id_plano = N.id_plano
INNER JOIN VENDEDOR V ON V.id_vendedor = N.id_vendedor
LEFT JOIN MARCA M ON M.id_marca = P.id_marca
INNER JOIN FORNECEDOR  F ON F.id_fornecedor = P.id_fornecedor
WHERE N.data_emissao between '{dataInicial}' AND '{dataFinal}'
and N.INUTILIZADA = 'N'
 and (not (CODIGO_STATUS in (101,151,218,110,205,301,302,303)))
 and N.AMBIENTE = 1

UNION ALL

SELECT
IC.ID_ITEM_CUPOM AS ID,
cu.cup_numero,
cu.cup_data,
cl.cliente,
p.descricao,
ic.itc_cfop,
IC.itc_quantidade,
IC.itc_cst,
PV.descricao AS DESCRICAO_PLANO ,
v.nome,
ic.itc_und,
ic.itc_preco_liquido,
p.ncm,
ic.itc_preco_tabela,
p.grupo,
p.referencia,
ic.itc_bc_icms,
ic.itc_subtotal,
ic.itc_aliquota_icms,
ic.itc_subtotal_bruto,
ic.itc_valor_desconto,
m.marca,
F.razao_soc,
'V' AS OPERACAO,
'CUPOM' AS TIPO_SAIDA
FROM item_cupom ic inner join cupom cu ON ic.id_cupom=cu.id_cupom
inner join cliente cl on cl.id_cliente = cu.id_cliente
inner join produtos p on p.codigo = ic.itc_codigo
inner join vendedor v on v.id_vendedor = cu.id_vendedor
inner join fornecedor f on f.id_fornecedor = p.id_fornecedor
LEFT join marca m on m.id_marca = p.id_marca
inner join plano_venda PV ON PV.id_plano = CU.id_plano
where CU.CUP_DATA between '{dataInicial}' and '{dataFinal}'
 and cu.CUP_CONCLUIDO='S' and cu.cup_cancelado='N'
 and ic.itc_cancelado != 'S'";

            var consulta = _dbContext.ProdutosVendidos.FromSqlRaw(sql).ToImmutableList();
            return consulta.OrderByDescending(p => p.DataVenda).ToImmutableList();            
        }

        public ImmutableList<MEDICAMENTOS> ExibeTodosMedicamentos(string dataInicial, string dataFinal)
        {
            string sql = @$"
 SELECT
S.data_venda,
S.nota_fiscal,
S.CODIGO,
P.descricao,
S.cfop,
S.quantidade_vendida,
L.lote,
L.FABRICACAO,
L.VENCIMENTO,
S.preco_venda,
S.desconto,
PV.descricao AS DESCRICAO_PLANO,
C.cliente,
V.nome,
M.marca,
G.grupo,
F.razao_soc,
S.preco_custo,
S.preco_venda_bruto,
P.referencia,
S.N_SUB_TOTAL,
S.OPERACAO,
'NFE' AS TIPO_SAIDA
FROM SAIDAS S INNER JOIN produtos P ON S.codigo = P.codigo
INNER JOIN plano_venda PV ON S.id_plano = PV.id_plano
INNER JOIN cliente C ON C.id_cliente = S.id_cliente
INNER JOIN VENDEDOR V ON V.id_vendedor = S.id_vendedor
INNER JOIN MARCA M ON M.id_marca = P.ID_MARCA
INNER JOIN GRUPO G ON G.id_grupo = P.id_grupo
INNER JOIN LOTE L ON L.registro = S.registro
INNER JOIN FORNECEDOR F ON F.ID_FORNECEDOR = S.ID_FORNECEDOR
WHERE S.data_venda between '{dataInicial}' AND '{dataFinal}'
AND S.quantidade_vendida > 0 AND S.NUMERO_CUPOM=''";

            var consulta = _dbContext.MedicamentosVendidos.FromSqlRaw(sql).ToImmutableList();
            return consulta.OrderByDescending(m => m.DataVenda).ToImmutableList();
        }

        public ImmutableList<COMBUSTIVEIS> ExibeTodosCombustiveis(string dataInicial, string dataFinal)
        {
            string sql = @$"SELECT
N.data_emissao,
C.cliente,
P.descricao,
I.quantidade,
I.preco_venda,
PV.descricao AS DESCRICAO_PLANO,
V.nome,
P.unidade,
I.desconto_val_subtotal,
I.bico,
I.bomba,
I.tanque,
T.trn_descricaO,
N.VEICULO_PLACA,
N.VEICULO_HODOMETRO,
N.VINCULADA,
N.DATA_VINCULAMENTO,
N.NUMERO,
A.HORA
FROM NFCE N INNER JOIN item_nfce I ON N.id_nfce = I.id_nfce
INNER JOIN produtos P ON P.codigo = I.codigo
INNER JOIN plano_venda PV ON N.id_plano = PV.id_plano
INNER JOIN cliente C ON C.id_cliente = N.id_cliente
INNER JOIN VENDEDOR V ON V.id_vendedor = N.id_vendedor
INNER JOIN turnos T ON T.id_turno = T.id_turno
INNER JOIN grupo G ON G.id_grupo = P.id_grupo
INNER JOIN abastecimento A on A.id_ab = I.id_ab
WHERE N.data_emissao between '{dataInicial}' AND '{dataFinal}'
AND G.grupo LIKE 'COMBUS%'
AND I.quantidade > 0";

            var consulta = _dbContext.CombustiveisVendidos.FromSqlRaw(sql).ToImmutableList();
            return consulta.OrderByDescending(m => m.DataEmissao).ToImmutableList();
        }
    }
}
