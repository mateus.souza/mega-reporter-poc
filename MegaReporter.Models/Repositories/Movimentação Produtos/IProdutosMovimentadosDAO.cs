﻿using MegaReporter.Models.Entities;
using System.Collections.Immutable;

namespace MegaReporter.Models.Repositories
{
    public interface IProdutosMovimentadosDAO<T>
    {
        ImmutableList<T> ExibeTodos(string dataInicial, string dataFinal);

        ImmutableList<MEDICAMENTOS> ExibeTodosMedicamentos(string dataInicial, string dataFinal);
        ImmutableList<COMBUSTIVEIS> ExibeTodosCombustiveis(string dataInicial, string dataFinal);
    }
}
