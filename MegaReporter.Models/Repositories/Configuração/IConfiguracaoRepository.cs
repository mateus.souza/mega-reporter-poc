﻿using MegaReporter.Models.Entities.Loja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Repositories.Configuração
{
    public interface IConfiguracaoRepository
    {
        public bool ComercializaMedicamento();

        public bool ComercializaCombustivel();

        public LOJA GetDadosLoja();

    }
}
