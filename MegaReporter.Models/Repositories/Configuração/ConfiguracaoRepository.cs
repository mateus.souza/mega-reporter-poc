﻿using MegaReporter.Models.Entities.Configurações_Loja;
using MegaReporter.Models.Entities.Loja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Repositories.Configuração
{
    public class ConfiguracaoRepository : IConfiguracaoRepository
    {
        private readonly MegaReporterDbContext _dbContext;

        public ConfiguracaoRepository(MegaReporterDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool ComercializaMedicamento()
        {
            var consulta = _dbContext.Configuracoes.FirstOrDefault()?.Medicamento;

            if (consulta == "N")
            {
                return false;
            }
            return true;
        }

        public bool ComercializaCombustivel()
        {
            var consulta = _dbContext.Configuracoes.FirstOrDefault()?.Combustivel;

            if (consulta == "N")
            {
                return false;
            }
            return true;
        }

        public LOJA GetDadosLoja()
        {
            return _dbContext.Loja.FirstOrDefault()!;
        }
    }
}
