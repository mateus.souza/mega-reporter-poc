﻿using MegaReporter.Models.Entities;
using MegaReporter.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.Models.Controllers
{
    public class MovimentacaoProdutosController
    {
        private readonly IProdutosMovimentadosDAO<PRODUTOS> _daoProdutosVendidos;

        public MovimentacaoProdutosController(IProdutosMovimentadosDAO<PRODUTOS> daoProdutosVendidos)
        {
            _daoProdutosVendidos = daoProdutosVendidos;
        }

        public ImmutableList<PRODUTOS> ExibeTodos(string dataInicial, string dataFinal)
        {
            return _daoProdutosVendidos.ExibeTodos(dataInicial, dataFinal);
        }

        public ImmutableList<MEDICAMENTOS> ExibeTodosMedicamentos(string dataInicial, string dataFinal)
        {
            return _daoProdutosVendidos.ExibeTodosMedicamentos(dataInicial, dataFinal);
        }

        public ImmutableList<COMBUSTIVEIS> ExibeTodosCombustiveis(string dataInicial, string dataFinal)
        {
            return _daoProdutosVendidos.ExibeTodosCombustiveis(dataInicial, dataFinal);
        }
    }
}
