﻿using MegaReporter.Models.Entities.Loja;
using MegaReporter.Models.Repositories.Configuração;

namespace MegaReporter.Models.Controllers
{
    public class ConfiguracoesController
    {
        
        private readonly IConfiguracaoRepository _configuracaoRepository;

        public ConfiguracoesController(IConfiguracaoRepository configuracaoRepository)
        {
            _configuracaoRepository = configuracaoRepository;
        }
        
        public bool ComercializaMedicamento()
        {
            return _configuracaoRepository.ComercializaMedicamento();
        }

        public bool ComercializaCombustivel()
        {
            return _configuracaoRepository.ComercializaCombustivel();
        }

        public LOJA GetDadosLoja()
        {
            return _configuracaoRepository.GetDadosLoja();
        }
    }
}
