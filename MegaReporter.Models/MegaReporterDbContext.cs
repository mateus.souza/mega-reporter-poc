﻿using FirebirdSql.Data.FirebirdClient;
using MegaReporter.Models.Entities;
using MegaReporter.Models.Entities.Configurações_Loja;
using MegaReporter.Models.Entities.Loja;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Text;

namespace MegaReporter.Models
{
    public class MegaReporterDbContext : DbContext
    {
        public DbSet<PRODUTOS> ProdutosVendidos { get; set; } = null!;
        public DbSet<MEDICAMENTOS> MedicamentosVendidos { get; set; } = null!;
        public DbSet<COMBUSTIVEIS> CombustiveisVendidos { get; set; } = null!;
        public DbSet<CONFIGURACOES> Configuracoes { get; set; } = null!;
        public DbSet<LOJA> Loja { get; set; } = null!;


        public MegaReporterDbContext(DbContextOptions options) : base(options)
        {

        }
        
        public MegaReporterDbContext(string connectionString) : base 
            (new DbContextOptionsBuilder().UseFirebird(connectionString).Options) { }

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var enc1252 = Encoding.GetEncoding(1252);
        }
    }
       
        
}
