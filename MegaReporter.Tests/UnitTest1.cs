using MegaReporter.Models;
using MegaReporter.Models.Entities;
using MegaReporter.Models.Repositories;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace MegaReporter.Tests
{
    public class UnitTest1
    {
        private readonly DbConnection _connection;
        private readonly DbContextOptions<MegaReporterDbContext> _contextOptions;

        public UnitTest1()
        {
            _connection = new SqliteConnection("Filename=:memory:");
            _connection.Open();

            _contextOptions = new DbContextOptionsBuilder<MegaReporterDbContext>()
                .UseInMemoryDatabase(databaseName: "teste_aplicacao")
                .Options;
        }

        MegaReporterDbContext CreateContext()
        {
            var contexto = new MegaReporterDbContext(_contextOptions);
            return contexto;
        }

        [Fact]
        public void Test1()
        {
            var context = CreateContext();
            var repository = new ProdutosMovimentadosDAO(context);

            var esperado = new List<PRODUTOS>()
            {
                new PRODUTOS
                {
                    Codigo = "1",
                    Descricao = "Produto Teste",
                    IdTipo = 1,
                    PrecoVenda = 10,
                    QuantidadeVendida = 10,
                    Unidade = "UN",
                    //ValorTotal = 5.10
                }
            };

            context.ProdutosVendidos.AddRange(esperado);
            context.SaveChanges();

            var retorno = repository.ExibeTodos();
            Assert.Equal(esperado, retorno);
        }
    }
}