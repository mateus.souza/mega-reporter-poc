﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.View
{
    internal static class ConexaoBanco
    {
        public static string StringConexao()
        {
            IniFile iniFile = new IniFile("C:\\MFX\\dbxconnections.ini");

            var ipMaquina = iniFile.Read("IP", "IP_Server");

            string conexao = "";

            if (String.IsNullOrEmpty(ipMaquina))
            {
                conexao = "C:\\MFX\\DADOS\\MFX.FDB";
            }
            else
            {
                conexao = ipMaquina;
            }

            return conexao;
        }

    }
}
