﻿using MegaReporter.Models.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MegaReporter.View
{
    public partial class Form2 : Form
    {
        private readonly MovimentacaoProdutosController _produtosVendidosPorPlanoController;
        public Form2(MovimentacaoProdutosController produtosVendidosPorPlanoController)
        {
            InitializeComponent();
            _produtosVendidosPorPlanoController = produtosVendidosPorPlanoController;
            Cursor.Current = Cursors.WaitCursor;

            var consulta = _produtosVendidosPorPlanoController.ExibeTodos("01.01.2021", "06.10.2022");
            pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = consulta;



            Cursor.Current = Cursors.Default;
        }
    }
}
