﻿namespace MegaReporter.View
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.pRODUTOSVENDIDOSPORPLANOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVenda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorIcms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAliquota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCst = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesconto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNcm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoCusto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRODUTOSVENDIDOSPORPLANOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl",
            "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl",
            "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"});
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(800, 450);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(798, 425);
            this.xtraTabPage1.Text = "xtraTabPage1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.pRODUTOSVENDIDOSPORPLANOBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(798, 425);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // pRODUTOSVENDIDOSPORPLANOBindingSource
            // 
            this.pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = typeof(MegaReporter.Models.Entities.PRODUTOS);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodigo,
            this.colDataVenda,
            this.colCliente,
            this.colDescricao,
            this.colPrecoBruto,
            this.colValorIcms,
            this.colAliquota,
            this.colCst,
            this.colNome,
            this.colDesconto,
            this.colNcm,
            this.colPrecoCusto,
            this.colUnidade,
            this.colDescricaoPlano,
            this.colQuantidadeVendida,
            this.colPrecoVenda,
            this.colTipoSaida,
            this.colCfop,
            this.colValorTotal});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colCodigo
            // 
            this.colCodigo.FieldName = "Codigo";
            this.colCodigo.Name = "colCodigo";
            this.colCodigo.Visible = true;
            this.colCodigo.VisibleIndex = 0;
            // 
            // colDataVenda
            // 
            this.colDataVenda.FieldName = "DataVenda";
            this.colDataVenda.Name = "colDataVenda";
            this.colDataVenda.Visible = true;
            this.colDataVenda.VisibleIndex = 1;
            // 
            // colCliente
            // 
            this.colCliente.FieldName = "Cliente";
            this.colCliente.Name = "colCliente";
            this.colCliente.Visible = true;
            this.colCliente.VisibleIndex = 2;
            // 
            // colDescricao
            // 
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.Visible = true;
            this.colDescricao.VisibleIndex = 3;
            // 
            // colPrecoBruto
            // 
            this.colPrecoBruto.FieldName = "PrecoBruto";
            this.colPrecoBruto.Name = "colPrecoBruto";
            this.colPrecoBruto.Visible = true;
            this.colPrecoBruto.VisibleIndex = 4;
            // 
            // colValorIcms
            // 
            this.colValorIcms.FieldName = "ValorIcms";
            this.colValorIcms.Name = "colValorIcms";
            this.colValorIcms.Visible = true;
            this.colValorIcms.VisibleIndex = 5;
            // 
            // colAliquota
            // 
            this.colAliquota.FieldName = "Aliquota";
            this.colAliquota.Name = "colAliquota";
            this.colAliquota.Visible = true;
            this.colAliquota.VisibleIndex = 6;
            // 
            // colCst
            // 
            this.colCst.FieldName = "Cst";
            this.colCst.Name = "colCst";
            this.colCst.Visible = true;
            this.colCst.VisibleIndex = 7;
            // 
            // colNome
            // 
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 8;
            // 
            // colDesconto
            // 
            this.colDesconto.FieldName = "Desconto";
            this.colDesconto.Name = "colDesconto";
            this.colDesconto.Visible = true;
            this.colDesconto.VisibleIndex = 9;
            // 
            // colNcm
            // 
            this.colNcm.FieldName = "Ncm";
            this.colNcm.Name = "colNcm";
            this.colNcm.Visible = true;
            this.colNcm.VisibleIndex = 10;
            // 
            // colPrecoCusto
            // 
            this.colPrecoCusto.FieldName = "PrecoCusto";
            this.colPrecoCusto.Name = "colPrecoCusto";
            this.colPrecoCusto.Visible = true;
            this.colPrecoCusto.VisibleIndex = 11;
            // 
            // colUnidade
            // 
            this.colUnidade.FieldName = "Unidade";
            this.colUnidade.Name = "colUnidade";
            this.colUnidade.Visible = true;
            this.colUnidade.VisibleIndex = 12;
            // 
            // colDescricaoPlano
            // 
            this.colDescricaoPlano.FieldName = "DescricaoPlano";
            this.colDescricaoPlano.Name = "colDescricaoPlano";
            this.colDescricaoPlano.Visible = true;
            this.colDescricaoPlano.VisibleIndex = 13;
            // 
            // colQuantidadeVendida
            // 
            this.colQuantidadeVendida.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida.Name = "colQuantidadeVendida";
            this.colQuantidadeVendida.Visible = true;
            this.colQuantidadeVendida.VisibleIndex = 14;
            // 
            // colPrecoVenda
            // 
            this.colPrecoVenda.FieldName = "PrecoVenda";
            this.colPrecoVenda.Name = "colPrecoVenda";
            this.colPrecoVenda.Visible = true;
            this.colPrecoVenda.VisibleIndex = 15;
            // 
            // colTipoSaida
            // 
            this.colTipoSaida.FieldName = "TipoSaida";
            this.colTipoSaida.Name = "colTipoSaida";
            this.colTipoSaida.Visible = true;
            this.colTipoSaida.VisibleIndex = 16;
            // 
            // colCfop
            // 
            this.colCfop.FieldName = "Cfop";
            this.colCfop.Name = "colCfop";
            this.colCfop.Visible = true;
            this.colCfop.VisibleIndex = 17;
            // 
            // colValorTotal
            // 
            this.colValorTotal.FieldName = "ValorTotal";
            this.colValorTotal.Name = "colValorTotal";
            this.colValorTotal.OptionsColumn.ReadOnly = true;
            this.colValorTotal.Visible = true;
            this.colValorTotal.VisibleIndex = 18;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(798, 428);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(798, 428);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRODUTOSVENDIDOSPORPLANOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private BindingSource pRODUTOSVENDIDOSPORPLANOBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto;
        private DevExpress.XtraGrid.Columns.GridColumn colValorIcms;
        private DevExpress.XtraGrid.Columns.GridColumn colAliquota;
        private DevExpress.XtraGrid.Columns.GridColumn colCst;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colDesconto;
        private DevExpress.XtraGrid.Columns.GridColumn colNcm;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoCusto;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop;
        private DevExpress.XtraGrid.Columns.GridColumn colValorTotal;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    }
}