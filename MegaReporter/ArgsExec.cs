﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaReporter.View
{
    internal class ArgsExec
    {
        
        private List<string> RecebeParametros()
        {
            List<string> lista = new List<string>();

            string[] passedInArgs = Environment.GetCommandLineArgs();

            foreach (string s in passedInArgs)
            {
                lista.Add(s);
            }
            lista.RemoveAt(0);
            return lista;
        }

        public string AbreFormulario()
        {
            var nomeFormulario = RecebeParametros().FirstOrDefault();
            
            if (String.IsNullOrEmpty(nomeFormulario))
            {
                MessageBox.Show("Não foi possível abrir o formulário desejado!");
                throw new Exception("Não foi possível abrir o formulário desejado!");
            }

            return nomeFormulario;
        }
        
    }
}
