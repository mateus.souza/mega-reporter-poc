﻿using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace MegaReporter.View
{
    internal class DevExpressSettings
    {

        public DevExpressSettings()
        {

        }

        private void EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.Tag != null && e.Button.Tag.ToString() == "refresh")
            {
                // load records  
            }
        }
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            SendKeys.Send("{Right}");
        }


        string pageFooter = @"{\rtf1\deff0{\fonttbl{\f0 Calibri;}
{\f1\fcharset204 Times New Roman;}{\f2\fcharset204 Helvetica Neue;}
}{\colortbl ;\red0\green0\blue255 ;\red51\green51\blue51 ;\red255\green255\blue255 ;}
{\*\defchp \f1}{\stylesheet {\ql\f1 Normal;}
{\*\cs1\f1 Default Paragraph Font;}{\*\cs2\sbasedon1\f1 Line Number;}
{\*\cs3\ul\f1\cf1 Hyperlink;}{\*\ts4\tsrowd\f1\ql\trautofit1\tscellpaddfl3\tscellpaddl108\tscellpaddfr3\tscellpaddr108\tsvertalt\cltxlrtb Normal Table;}
{\*\ts5\tsrowd\sbasedon4\f1\ql\trbrdrt\brdrs\brdrw10\trbrdrl\brdrs\brdrw10\trbrdrb\brdrs\brdrw10\trbrdrr\brdrs\brdrw10\trbrdrh\brdrs\brdrw10\trbrdrv\brdrs\brdrw10\trautofit1\tscellpaddfl3\tscellpaddl108\tscellpaddfr3\tscellpaddr108\tsvertalt\cltxlrtb Table Simple 1;}}
{\*\listoverridetable}\nouicompat\splytwnine\htmautsp\sectd\marglsxn0\margrsxn0\pard\plain\ql{\f2\fs18\cf2\chcbpat3 [Page }{\field{\*\fldinst{\f2\fs18\cf2\chcbpat3 DOCVARIABLE PAGE ""\{0\}""}}{\fldrslt{\f2\fs18\cf2\chcbpat3 PAGE}}}{\f2\fs18\cf2\chcbpat3  of Pages }
{\field{\*\fldinst{\f2\fs18\cf2\chcbpat3 DOCVARIABLE NUMPAGES ""\{0\}""}}{\fldrslt{\f2\fs18\cf2\chcbpat3 NUMPAGES}}}{\f2\fs18\cf2\chcbpat3 ]}";


        private void gridView1_CustomColumnDisplayText(object sender,
               CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "V") e.DisplayText = "Venda";

            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "D") e.DisplayText = "Devolução";

            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "S") e.DisplayText = "Simples Remessa";


            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "T") e.DisplayText = "Transferência";

            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "I") e.DisplayText = "Imobilizado";

            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "C") e.DisplayText = "Consumo";

            if (e.Column.FieldName == "Operacao")
                if (Convert.ToString(e.Value) == "O") e.DisplayText = "Outras Operações";
        }

        public void ConfiguraImpressaoeGrid(Exportacao exportacao, string TituloDocumento, 
            string Subtitulo, GridView gridView, 
            GridControl gridControl, string nomeLayout)
        {
            //Adiciona titulo e subtitulo na página
            exportacao.GridControlComponent = gridControl;
            exportacao.TituloDocumento = TituloDocumento;
            exportacao.SubTitulo = Subtitulo;

            //Count de grid
            gridControl.UseEmbeddedNavigator = true;
            gridControl.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_ButtonClick;
            var button = gridControl.EmbeddedNavigator.Buttons.CustomButtons.Add();
            button.Tag = "refresh";

            //Ajusta colunas de Grid
            gridView.OptionsView.ColumnAutoWidth = true;
            
            
            gridView.OptionsPrint.AutoWidth = true;

            //Soma valores de agrupamento de grid
            GridGroupSummaryItem item1 = new GridGroupSummaryItem();
            item1.FieldName = "TotalLiquido";
            item1.SummaryType = SummaryItemType.Sum;
            item1.DisplayFormat = "{0:c2}";
            item1.ShowInGroupColumnFooter = gridView.Columns["TotalLiquido"];
            gridView.GroupSummary.Add(item1);

            //Adiciona contador de páginas em impressão de relatório
            gridView.OptionsPrint.RtfPageFooter = pageFooter;

            //Adiciona layout para grid

            /*
            try
            {
                gridView.RestoreLayoutFromXml(nomeLayout);
            }
            catch
            {
                throw new Exception("Ocorreu um erro ao carregar layout do grid, favor entre em contato com o suporte técnico.");
            }
            */
            

            gridView.CustomColumnDisplayText += gridView1_CustomColumnDisplayText;

            //Pinta colunas 
            CustomDrawGroupRow(gridControl, gridView);

           
        }

       

        public static void CustomDrawGroupRow(GridControl gridControl, GridView gridView)
        {
            //gridView.Columns["TotalLiquido"].Group();

            // Handle this event to paint group rows manually
            gridView.CustomDrawGroupRow += (s, e) =>
            {
                if (e.RowHandle % 2 == 0)
                {
                    e.Appearance.BackColor = Color.BlanchedAlmond;
                    e.Appearance.ForeColor = Color.DimGray; ;
                }
            };
        }

        public void CaminhaDataParaDireita(DateTimePicker dataInicial, DateTimePicker dataFinal)
        {
            dataInicial.ValueChanged += dateTimePicker1_ValueChanged;
            dataFinal.ValueChanged += dateTimePicker1_ValueChanged;
        }
    }
}
