﻿using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Financeiro.View2.Forms.Base
{
    public partial class FrmErro : XtraForm
    {
        public FrmErro(Exception ex)
        {
            InitializeComponent();

            txtTipoErro.BackColor = this.BackColor;
            txtMensagemErro.BackColor = this.BackColor;
            txtInnerException.BackColor = this.BackColor;
           


            txtTipoErro.Text = ex.GetType().ToString();

            txtMensagemErro.Text = ex.Message;

            string mensagemCompleta = ex.InnerException?.InnerException?.Message;
            if (string.IsNullOrEmpty(mensagemCompleta))
                mensagemCompleta = ex.InnerException?.Message;

            txtInnerException.Text = mensagemCompleta;

            txtStackTrace.Text = "************** Rastreamento do Erro **************"+Environment.NewLine+ex.StackTrace;


            pictureBox1.Image = SystemIcons.Error.ToBitmap();
            this.IconOptions.Icon = SystemIcons.Error;

            this.BringToFront();



            //SkinElement element1 = SkinManager.GetSkinElement(SkinProductId.Ribbon, UserLookAndFeel.Default, "TabHeaderBackground");
            //SkinElement element2 = SkinManager.GetSkinElement(SkinProductId.Ribbon, UserLookAndFeel.Default, RibbonSkins.SkinFormCaption);
            //element1.Color.SolidImageCenterColor = Color.Red;
            //element2.Color.SolidImageCenterColor = Color.Red;
            //LookAndFeelHelper.ForceDefaultLookAndFeelChanged();

            this.Shown += FrmErro_Shown;
        }

        private void FrmErro_Shown(object sender, EventArgs e)
        {
            this.BringToFront();
            this.TopMost = true;
        }

        private void txtTipoErro_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
