﻿
namespace Financeiro.View2.Forms.Base
{
    partial class FrmErro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmErro));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtMensagemErro = new System.Windows.Forms.TextBox();
            this.txtStackTrace = new System.Windows.Forms.TextBox();
            this.lbTipoExcecao = new System.Windows.Forms.Label();
            this.txtTipoErro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInnerException = new System.Windows.Forms.TextBox();
            this.pnlTopo = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlTipoErro = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTopo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlTipoErro.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(8, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 49);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtMensagemErro
            // 
            this.txtMensagemErro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensagemErro.Location = new System.Drawing.Point(116, 3);
            this.txtMensagemErro.Multiline = true;
            this.txtMensagemErro.Name = "txtMensagemErro";
            this.txtMensagemErro.ReadOnly = true;
            this.txtMensagemErro.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMensagemErro.Size = new System.Drawing.Size(469, 40);
            this.txtMensagemErro.TabIndex = 2;
            // 
            // txtStackTrace
            // 
            this.txtStackTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStackTrace.Location = new System.Drawing.Point(0, 137);
            this.txtStackTrace.Multiline = true;
            this.txtStackTrace.Name = "txtStackTrace";
            this.txtStackTrace.ReadOnly = true;
            this.txtStackTrace.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStackTrace.Size = new System.Drawing.Size(668, 132);
            this.txtStackTrace.TabIndex = 3;
            // 
            // lbTipoExcecao
            // 
            this.lbTipoExcecao.AutoSize = true;
            this.lbTipoExcecao.Location = new System.Drawing.Point(47, 10);
            this.lbTipoExcecao.Name = "lbTipoExcecao";
            this.lbTipoExcecao.Size = new System.Drawing.Size(69, 13);
            this.lbTipoExcecao.TabIndex = 4;
            this.lbTipoExcecao.Text = "Tipo do Erro:";
            // 
            // txtTipoErro
            // 
            this.txtTipoErro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTipoErro.Location = new System.Drawing.Point(116, 7);
            this.txtTipoErro.Name = "txtTipoErro";
            this.txtTipoErro.ReadOnly = true;
            this.txtTipoErro.Size = new System.Drawing.Size(469, 21);
            this.txtTipoErro.TabIndex = 5;
            this.txtTipoErro.TextChanged += new System.EventHandler(this.txtTipoErro_TextChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 37);
            this.label1.TabIndex = 6;
            this.label1.Text = "Descrição Resumida do Erro:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Location = new System.Drawing.Point(73, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 29);
            this.label2.TabIndex = 7;
            this.label2.Text = "Descrição Completa do Erro:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtInnerException
            // 
            this.txtInnerException.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInnerException.Location = new System.Drawing.Point(186, 82);
            this.txtInnerException.Multiline = true;
            this.txtInnerException.Name = "txtInnerException";
            this.txtInnerException.ReadOnly = true;
            this.txtInnerException.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInnerException.Size = new System.Drawing.Size(469, 41);
            this.txtInnerException.TabIndex = 8;
            // 
            // pnlTopo
            // 
            this.pnlTopo.Controls.Add(this.panel3);
            this.pnlTopo.Controls.Add(this.txtInnerException);
            this.pnlTopo.Controls.Add(this.pnlTipoErro);
            this.pnlTopo.Controls.Add(this.label2);
            this.pnlTopo.Controls.Add(this.panel2);
            this.pnlTopo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopo.Location = new System.Drawing.Point(0, 0);
            this.pnlTopo.Name = "pnlTopo";
            this.pnlTopo.Size = new System.Drawing.Size(668, 137);
            this.pnlTopo.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtMensagemErro);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(70, 33);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(598, 46);
            this.panel3.TabIndex = 4;
            // 
            // pnlTipoErro
            // 
            this.pnlTipoErro.Controls.Add(this.lbTipoExcecao);
            this.pnlTipoErro.Controls.Add(this.txtTipoErro);
            this.pnlTipoErro.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTipoErro.Location = new System.Drawing.Point(70, 0);
            this.pnlTipoErro.Name = "pnlTipoErro";
            this.pnlTipoErro.Size = new System.Drawing.Size(598, 33);
            this.pnlTipoErro.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(70, 137);
            this.panel2.TabIndex = 2;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnSair);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 269);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Padding = new System.Windows.Forms.Padding(5);
            this.pnlBottom.Size = new System.Drawing.Size(668, 45);
            this.pnlBottom.TabIndex = 10;
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.DarkRed;
            this.btnSair.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSair.ForeColor = System.Drawing.Color.White;
            this.btnSair.Location = new System.Drawing.Point(5, 5);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(658, 35);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "X  Sair";
            this.btnSair.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // FrmErro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 314);
            this.Controls.Add(this.txtStackTrace);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTopo);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FrmErro.IconOptions.Icon")));
            this.LookAndFeel.SkinMaskColor = System.Drawing.Color.Maroon;
            this.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Maroon;
            this.LookAndFeel.SkinName = "The Bezier";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "FrmErro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Erro";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTopo.ResumeLayout(false);
            this.pnlTopo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlTipoErro.ResumeLayout(false);
            this.pnlTipoErro.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtMensagemErro;
        private System.Windows.Forms.TextBox txtStackTrace;
        private System.Windows.Forms.Label lbTipoExcecao;
        private System.Windows.Forms.TextBox txtTipoErro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInnerException;
        private System.Windows.Forms.Panel pnlTopo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlTipoErro;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSair;
    }
}