﻿namespace MegaReporter.View
{
    partial class Exportacao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exportacao));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDOCX = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHTML = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMHT = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPDF = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRTF = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTEXT = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuXLS = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuXLSX = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.itemDOCX = new DevExpress.XtraBars.BarButtonItem();
            this.itemRTF = new DevExpress.XtraBars.BarButtonItem();
            this.itemTXT = new DevExpress.XtraBars.BarButtonItem();
            this.itemHTML = new DevExpress.XtraBars.BarButtonItem();
            this.itemMHT = new DevExpress.XtraBars.BarButtonItem();
            this.itemPDF = new DevExpress.XtraBars.BarButtonItem();
            this.itemCSV = new DevExpress.XtraBars.BarButtonItem();
            this.itemXLS = new DevExpress.XtraBars.BarButtonItem();
            this.itemXLSX = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnExportar = new DevExpress.XtraEditors.DropDownButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnVisualizarImpressao = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCSV,
            this.mnuDOCX,
            this.mnuHTML,
            this.mnuMHT,
            this.mnuPDF,
            this.mnuRTF,
            this.mnuTEXT,
            this.mnuXLS,
            this.mnuXLSX});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(107, 202);
            // 
            // mnuCSV
            // 
            this.mnuCSV.Name = "mnuCSV";
            this.mnuCSV.Size = new System.Drawing.Size(106, 22);
            this.mnuCSV.Text = "CSV";
            // 
            // mnuDOCX
            // 
            this.mnuDOCX.Name = "mnuDOCX";
            this.mnuDOCX.Size = new System.Drawing.Size(106, 22);
            this.mnuDOCX.Text = "DOCX";
            // 
            // mnuHTML
            // 
            this.mnuHTML.Name = "mnuHTML";
            this.mnuHTML.Size = new System.Drawing.Size(106, 22);
            this.mnuHTML.Text = "HTML";
            // 
            // mnuMHT
            // 
            this.mnuMHT.Name = "mnuMHT";
            this.mnuMHT.Size = new System.Drawing.Size(106, 22);
            this.mnuMHT.Text = "MHT";
            // 
            // mnuPDF
            // 
            this.mnuPDF.Name = "mnuPDF";
            this.mnuPDF.Size = new System.Drawing.Size(106, 22);
            this.mnuPDF.Text = "PDF";
            // 
            // mnuRTF
            // 
            this.mnuRTF.Name = "mnuRTF";
            this.mnuRTF.Size = new System.Drawing.Size(106, 22);
            this.mnuRTF.Text = "RTF";
            // 
            // mnuTEXT
            // 
            this.mnuTEXT.Name = "mnuTEXT";
            this.mnuTEXT.Size = new System.Drawing.Size(106, 22);
            this.mnuTEXT.Text = "TEXT";
            // 
            // mnuXLS
            // 
            this.mnuXLS.Name = "mnuXLS";
            this.mnuXLS.Size = new System.Drawing.Size(106, 22);
            this.mnuXLS.Text = "XLS";
            // 
            // mnuXLSX
            // 
            this.mnuXLSX.Name = "mnuXLSX";
            this.mnuXLSX.Size = new System.Drawing.Size(106, 22);
            this.mnuXLSX.Text = "XLSX";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.itemDOCX),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemRTF),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemTXT),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemHTML),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemMHT),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemPDF),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemCSV),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemXLS),
            new DevExpress.XtraBars.LinkPersistInfo(this.itemXLSX)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.MenuCaption = "Exportar para...";
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // itemDOCX
            // 
            this.itemDOCX.Caption = ".DOCX";
            this.itemDOCX.Id = 1;
            this.itemDOCX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemDOCX.ImageOptions.Image")));
            this.itemDOCX.Name = "itemDOCX";
            // 
            // itemRTF
            // 
            this.itemRTF.Caption = ".RTF";
            this.itemRTF.Id = 5;
            this.itemRTF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemRTF.ImageOptions.Image")));
            this.itemRTF.Name = "itemRTF";
            // 
            // itemTXT
            // 
            this.itemTXT.Caption = ".TXT";
            this.itemTXT.Id = 6;
            this.itemTXT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemTXT.ImageOptions.Image")));
            this.itemTXT.Name = "itemTXT";
            // 
            // itemHTML
            // 
            this.itemHTML.Caption = ".HTML";
            this.itemHTML.Id = 2;
            this.itemHTML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemHTML.ImageOptions.Image")));
            this.itemHTML.Name = "itemHTML";
            // 
            // itemMHT
            // 
            this.itemMHT.Caption = ".MHT";
            this.itemMHT.Id = 3;
            this.itemMHT.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemMHT.ImageOptions.Image")));
            this.itemMHT.Name = "itemMHT";
            // 
            // itemPDF
            // 
            this.itemPDF.Caption = ".PDF";
            this.itemPDF.Id = 4;
            this.itemPDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemPDF.ImageOptions.Image")));
            this.itemPDF.Name = "itemPDF";
            // 
            // itemCSV
            // 
            this.itemCSV.Caption = ".CSV";
            this.itemCSV.Id = 0;
            this.itemCSV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemCSV.ImageOptions.Image")));
            this.itemCSV.Name = "itemCSV";
            // 
            // itemXLS
            // 
            this.itemXLS.Caption = ".XLS";
            this.itemXLS.Id = 7;
            this.itemXLS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemXLS.ImageOptions.Image")));
            this.itemXLS.Name = "itemXLS";
            // 
            // itemXLSX
            // 
            this.itemXLSX.Caption = ".XLSX";
            this.itemXLSX.Id = 8;
            this.itemXLSX.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("itemXLSX.ImageOptions.Image")));
            this.itemXLSX.Name = "itemXLSX";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Word", new System.Guid("311401d9-47a6-4150-8175-5d2ca159bb0d")),
            new DevExpress.XtraBars.BarManagerCategory("Excel", new System.Guid("98be41fc-265c-49b2-8221-98a59c0bb593")),
            new DevExpress.XtraBars.BarManagerCategory("Web", new System.Guid("d4203d91-60c3-4f3e-9d53-9c2e75600a93"))});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.itemCSV,
            this.itemDOCX,
            this.itemHTML,
            this.itemMHT,
            this.itemPDF,
            this.itemRTF,
            this.itemTXT,
            this.itemXLS,
            this.itemXLSX});
            this.barManager1.MaxItemId = 9;
            this.barManager1.PopupShowMode = DevExpress.XtraBars.PopupShowMode.Inplace;
            this.barManager1.ShowCloseButton = true;
            this.barManager1.ShowFullMenus = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Custom 2";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.barDockControlTop.Size = new System.Drawing.Size(84, 20);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 30);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.barDockControlBottom.Size = new System.Drawing.Size(84, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 20);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 10);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(84, 20);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 10);
            // 
            // btnExportar
            // 
            this.btnExportar.DropDownControl = this.popupMenu1;
            this.btnExportar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExportar.ImageOptions.Image")));
            this.btnExportar.Location = new System.Drawing.Point(-1, 2);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExportar.MenuManager = this.barManager1;
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.barManager1.SetPopupContextMenu(this.btnExportar, this.popupMenu1);
            this.btnExportar.Size = new System.Drawing.Size(47, 27);
            this.btnExportar.TabIndex = 32;
            this.toolTip1.SetToolTip(this.btnExportar, "Exportar para...");
            // 
            // btnVisualizarImpressao
            // 
            this.btnVisualizarImpressao.BackColor = System.Drawing.Color.Transparent;
            this.btnVisualizarImpressao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnVisualizarImpressao.FlatAppearance.BorderSize = 0;
            this.btnVisualizarImpressao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVisualizarImpressao.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizarImpressao.Image")));
            this.btnVisualizarImpressao.Location = new System.Drawing.Point(45, 1);
            this.btnVisualizarImpressao.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnVisualizarImpressao.Name = "btnVisualizarImpressao";
            this.btnVisualizarImpressao.Size = new System.Drawing.Size(41, 27);
            this.btnVisualizarImpressao.TabIndex = 31;
            this.toolTip1.SetToolTip(this.btnVisualizarImpressao, "Visualizar Impressão");
            this.btnVisualizarImpressao.UseVisualStyleBackColor = false;
            this.btnVisualizarImpressao.Click += new System.EventHandler(this.btnVisualizarImpressao_Click_1);
            // 
            // Exportacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.btnVisualizarImpressao);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Exportacao";
            this.Size = new System.Drawing.Size(84, 30);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem mnuCSV;
        private ToolStripMenuItem mnuDOCX;
        private ToolStripMenuItem mnuHTML;
        private ToolStripMenuItem mnuMHT;
        private ToolStripMenuItem mnuPDF;
        private ToolStripMenuItem mnuRTF;
        private ToolStripMenuItem mnuTEXT;
        private ToolStripMenuItem mnuXLS;
        private ToolStripMenuItem mnuXLSX;
        private SaveFileDialog saveFileDialog;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem itemDOCX;
        private DevExpress.XtraBars.BarButtonItem itemRTF;
        private DevExpress.XtraBars.BarButtonItem itemTXT;
        private DevExpress.XtraBars.BarButtonItem itemHTML;
        private DevExpress.XtraBars.BarButtonItem itemMHT;
        private DevExpress.XtraBars.BarButtonItem itemPDF;
        private DevExpress.XtraBars.BarButtonItem itemCSV;
        private DevExpress.XtraBars.BarButtonItem itemXLS;
        private DevExpress.XtraBars.BarButtonItem itemXLSX;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private ToolTip toolTip1;
        private DevExpress.XtraEditors.DropDownButton btnExportar;
        public Button btnVisualizarImpressao;
    }
}
