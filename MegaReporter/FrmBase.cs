﻿using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraRichEdit;

namespace MegaReporter.View
{
    public partial class FrmBase : DevExpress.XtraEditors.XtraForm
    {
        private Color CinzaPadrao = Color.FromArgb(160, 160, 160);

        public bool ConsiderarEnterComoTab
        {
            get; set;
        } = true;


        public FrmBase()
        {
            InitializeComponent();



            this.Shown += FrmBase_Shown1;

            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookAndFeel.SetSkinStyle(SkinStyle.Bezier);
            LookAndFeelHelper.ForceDefaultLookAndFeelChanged();

            SkinElement element1 = SkinManager.GetSkinElement(SkinProductId.Form, this.LookAndFeel, "FormCaption"); ;
            element1.Color.SolidImageCenterColor = Color.FromArgb(0, 48, 120);
            element1.Color.ForeColor = Color.White;

            StartPosition = FormStartPosition.CenterScreen;
        }


        private void FrmBase_Shown1(object sender, EventArgs e)
        {

            SetaCorItemSelecionado(this);
            SetaFormatDateTime(this);
        }


        private void FrmBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (ConsiderarEnterComoTab)
            {
                if (e.KeyCode == Keys.Enter)
                {

                    this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);

                }
                else if (e.KeyCode == Keys.Escape) //ESC
                {
                    this.SelectNextControl(this.ActiveControl, false, true, true, true);
                }
            }
        }

        public virtual void LimpaComponentes()
        {
            this.LimpaComponentes(this);
        }


        public virtual void HabilitaDesabilitaComponentes(Control parent, bool habilita)
        {
            foreach (Control ctrl in parent.Controls)
            {
                //Se o contorle for um TextBox...
                if (ctrl is FrmBase)
                {
                    ((FrmBase)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is RichEditControl)
                {
                    ((RichEditControl)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is Button)
                {
                    ((Button)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is DataGridView)
                {
                    ((DataGridView)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is GridControl)
                {
                    // ((GridControl)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is ComboBox)
                {
                    ((ComboBox)(ctrl)).Enabled = habilita;
                }
                else if (ctrl is ListView)
                {
                    ((ListView)(ctrl)).Enabled = habilita;
                }

                this.HabilitaDesabilitaComponentes(ctrl, habilita);
            }
        }

        public virtual void SetaCorItemSelecionado(Control parent)
        {
            foreach (Control ctrl in parent.Controls)
            {
                if (ctrl is GridControl)
                {
                    GridView gv = ((GridControl)(ctrl)).DefaultView as GridView;

                    if (gv != null)
                    {
                        gv.Appearance.FocusedRow.BackColor = Color.FromArgb(206, 221, 245);
                        gv.Appearance.FocusedRow.ForeColor = Color.Black;

                        if (gv.Editable == false)
                        {
                            gv.OptionsSelection.EnableAppearanceFocusedCell = false;
                        }
                    }
                }

                this.SetaCorItemSelecionado(ctrl);
            }
        }

        public virtual void SetaFormatDateTime(Control parent)
        {
            foreach (Control ctrl in parent.Controls)
            {
                if (ctrl is GridControl)
                {
                    GridView gv = ((GridControl)(ctrl)).DefaultView as GridView;


                    if (gv != null)
                    {
                        foreach (GridColumn col in gv.Columns)
                        {

                            if (col.Caption.ToUpper().Contains("DATA"))
                            {
                                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                                col.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
                            }
                        }

                    }
                }

                this.SetaCorItemSelecionado(ctrl);
            }
        }

        public virtual void LimpaComponentes(Control parent)
        {
            foreach (Control ctrl in parent.Controls)
            {
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
                else if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)(ctrl)).Value = DateTime.Now;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)(ctrl)).Text = String.Empty;
                }
                this.LimpaComponentes(ctrl);
            }

        }




        private void AjustaPanel()
        {
            foreach (Control control in this.Controls)
            {
                if (control is Panel)
                {
                    Panel pnl = (control as Panel);
                    if (pnl.Dock == DockStyle.Left)
                    {
                        pnl.BackColor = Color.Transparent;
                        pnl.Padding = new Padding(0, 0, 0, 0);
                        pnl.BorderStyle = BorderStyle.None;


                        pnl.Paint += Pnl_Paint;
                    }
                }
            }
        }

        private void Pnl_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, (sender as Panel).DisplayRectangle, CinzaPadrao, ButtonBorderStyle.Solid);
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

      
    }
}

