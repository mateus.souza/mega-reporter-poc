﻿namespace MegaReporter
{
    partial class FrmMovimentacaoProdutos
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMovimentacaoProdutos));
            this.pRODUTOSVENDIDOSPORPLANOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dataInicial = new System.Windows.Forms.DateTimePicker();
            this.dataFinal = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnEditar = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnCriarBusca = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.simpleButton1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.xpPageSelector1 = new DevExpress.Xpo.XPPageSelector(this.components);
            this.colCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mEDICAMENTOSVENDIDOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMBUSTIVEISVENDIDOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.entityInstantFeedbackSource1 = new DevExpress.Data.Linq.EntityInstantFeedbackSource();
            this.gridControlPostos = new DevExpress.XtraGrid.GridControl();
            this.gridViewPostos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDataEmissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescontoSubtotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBico = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBomba = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoTurno = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVeiculoPlaca = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVeiculoHodometro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVinculada = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVinculamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHora = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewPadrao3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodigo6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVenda5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCst4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNcm4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseCalculo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalLiquido5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalBruto4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDesconto4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferencia5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarca5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrupo5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperacao4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRazaoSocial4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorBaseIcms4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAliquota4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPadrao3 = new DevExpress.XtraGrid.GridControl();
            this.gridControlMedicamentos = new DevExpress.XtraGrid.GridControl();
            this.gridViewMedicamentos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDataVenda1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotaFiscal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodigo5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLote3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFabricacao3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVencimento3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDesconto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarca4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrupo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRazaoSocial3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoCusto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalLiquido4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperacao5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferencia4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewPadrao1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodigo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVenda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCst = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNcm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalLiquido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalBruto3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDesconto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferencia3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarca3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrupo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRazaoSocial1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorBaseIcms3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAliquota3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPadrao1 = new DevExpress.XtraGrid.GridControl();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridViewPadrao2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodigo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVenda2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCst1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNcm1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalLiquido1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalBruto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDesconto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferencia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarca = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrupo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperacao1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorBaseIcms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRazaoSocial2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAliquota = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPadrao2 = new DevExpress.XtraGrid.GridControl();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.gridControlGenêrico = new DevExpress.XtraGrid.GridControl();
            this.gridViewGenêrico = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodigo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataVenda3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCliente3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricao4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCst2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomeVendedor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNcm2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnidade4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescricaoPlano4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantidadeVendida4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoVenda4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoSaida4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCfop2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseCalculo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalLiquido2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalBruto1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalDesconto2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIcmsCredito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecoBruto3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferencia1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarca1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrupo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRazaoSocial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOperacao2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorBaseIcms1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValorAliquota1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pRODUTOSVENDIDOSPORPLANOBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mEDICAMENTOSVENDIDOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMBUSTIVEISVENDIDOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPostos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPostos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMedicamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMedicamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGenêrico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGenêrico)).BeginInit();
            this.SuspendLayout();
            // 
            // pRODUTOSVENDIDOSPORPLANOBindingSource
            // 
            this.pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = typeof(MegaReporter.Models.Entities.PRODUTOS);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dataInicial);
            this.panel2.Controls.Add(this.dataFinal);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(299, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(318, 28);
            this.panel2.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Periódo: ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // dataInicial
            // 
            this.dataInicial.CustomFormat = "dd.MM.yyyy";
            this.dataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataInicial.Location = new System.Drawing.Point(64, 3);
            this.dataInicial.Name = "dataInicial";
            this.dataInicial.Size = new System.Drawing.Size(108, 21);
            this.dataInicial.TabIndex = 7;
            this.dataInicial.Value = new System.DateTime(2022, 9, 27, 15, 15, 28, 0);
            // 
            // dataFinal
            // 
            this.dataFinal.CustomFormat = "dd.MM.yyyy";
            this.dataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataFinal.Location = new System.Drawing.Point(198, 3);
            this.dataFinal.Name = "dataFinal";
            this.dataFinal.Size = new System.Drawing.Size(108, 21);
            this.dataFinal.TabIndex = 9;
            this.dataFinal.Value = new System.DateTime(2022, 9, 27, 15, 15, 28, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(178, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "À";
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.RosyBrown;
            this.btnBuscar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(1170, 10);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(110, 40);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "       Pesquisar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.btnExcluir);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.btnEditar);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.btnCriarBusca);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.panel1.Location = new System.Drawing.Point(10, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1280, 62);
            this.panel1.TabIndex = 8;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label3);
            this.panel10.Controls.Add(this.comboBox1);
            this.panel10.Controls.Add(this.panel2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(533, 10);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(631, 40);
            this.panel10.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Modelo de busca: ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Padrão 1 - Todas as Vendas",
            "Padrão 2 - Por Plano de Pagamento",
            "Padrão 3 - Fins Contábeis",
            "Padrão 4 - Medicamentos",
            "Padrão 5 - Posto de Combustíveis",
            "Padrão 6 - Genêrico"});
            this.comboBox1.Location = new System.Drawing.Point(106, 10);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(187, 21);
            this.comboBox1.TabIndex = 17;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.comboBox1_SelectionChangeCommitted);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(353, 10);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(180, 40);
            this.panel11.TabIndex = 14;
            // 
            // btnExcluir
            // 
            this.btnExcluir.BackColor = System.Drawing.Color.RosyBrown;
            this.btnExcluir.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExcluir.FlatAppearance.BorderSize = 0;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            this.btnExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluir.Image")));
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(242, 10);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(111, 40);
            this.btnExcluir.TabIndex = 19;
            this.btnExcluir.Text = "     Excluir Layout";
            this.btnExcluir.UseVisualStyleBackColor = false;
            this.btnExcluir.Visible = false;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(232, 10);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 40);
            this.panel9.TabIndex = 13;
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.RosyBrown;
            this.btnEditar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEditar.FlatAppearance.BorderSize = 0;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditar.Location = new System.Drawing.Point(121, 10);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(111, 40);
            this.btnEditar.TabIndex = 18;
            this.btnEditar.Text = "      Editar Layout";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Visible = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(111, 10);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(10, 40);
            this.panel8.TabIndex = 12;
            // 
            // btnCriarBusca
            // 
            this.btnCriarBusca.BackColor = System.Drawing.Color.RosyBrown;
            this.btnCriarBusca.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCriarBusca.FlatAppearance.BorderSize = 0;
            this.btnCriarBusca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCriarBusca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(120)))));
            this.btnCriarBusca.Image = ((System.Drawing.Image)(resources.GetObject("btnCriarBusca.Image")));
            this.btnCriarBusca.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCriarBusca.Location = new System.Drawing.Point(0, 10);
            this.btnCriarBusca.Name = "btnCriarBusca";
            this.btnCriarBusca.Size = new System.Drawing.Size(111, 40);
            this.btnCriarBusca.TabIndex = 16;
            this.btnCriarBusca.Text = "        Criar busca         personalizada";
            this.btnCriarBusca.UseVisualStyleBackColor = false;
            this.btnCriarBusca.Click += new System.EventHandler(this.btnCriarBusca_Click);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1280, 10);
            this.panel4.TabIndex = 10;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 50);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1280, 12);
            this.panel7.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(844, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Save Layout";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(741, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Text = "button1";
            this.simpleButton1.UseVisualStyleBackColor = true;
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.simpleButton1);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(10, 62);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1280, 26);
            this.panel3.TabIndex = 9;
            // 
            // colCodigo
            // 
            this.colCodigo.Caption = "Código";
            this.colCodigo.FieldName = "Codigo";
            this.colCodigo.MinWidth = 17;
            this.colCodigo.Name = "colCodigo";
            this.colCodigo.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.True;
            this.colCodigo.Width = 64;
            // 
            // colDescricao
            // 
            this.colDescricao.AppearanceCell.Options.UseFont = true;
            this.colDescricao.Caption = "Descrição";
            this.colDescricao.FieldName = "Descricao";
            this.colDescricao.MinWidth = 17;
            this.colDescricao.Name = "colDescricao";
            this.colDescricao.Width = 208;
            // 
            // colQuantidadeVendida
            // 
            this.colQuantidadeVendida.Caption = "Quantidade";
            this.colQuantidadeVendida.DisplayFormat.FormatString = "0.0000";
            this.colQuantidadeVendida.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida.MinWidth = 17;
            this.colQuantidadeVendida.Name = "colQuantidadeVendida";
            this.colQuantidadeVendida.Width = 61;
            // 
            // colPrecoVenda
            // 
            this.colPrecoVenda.Caption = "Preço de Venda";
            this.colPrecoVenda.DisplayFormat.FormatString = "0.0000";
            this.colPrecoVenda.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoVenda.FieldName = "PrecoVenda";
            this.colPrecoVenda.MinWidth = 17;
            this.colPrecoVenda.Name = "colPrecoVenda";
            this.colPrecoVenda.Width = 77;
            // 
            // colUnidade
            // 
            this.colUnidade.Caption = "Unidade";
            this.colUnidade.FieldName = "Unidade";
            this.colUnidade.MinWidth = 17;
            this.colUnidade.Name = "colUnidade";
            this.colUnidade.Width = 39;
            // 
            // colDescricaoPlano
            // 
            this.colDescricaoPlano.FieldName = "DescricaoPlano";
            this.colDescricaoPlano.MinWidth = 17;
            this.colDescricaoPlano.Name = "colDescricaoPlano";
            this.colDescricaoPlano.Width = 64;
            // 
            // colTipoSaida
            // 
            this.colTipoSaida.Caption = "Tipo Saída";
            this.colTipoSaida.FieldName = "TipoSaida";
            this.colTipoSaida.Name = "colTipoSaida";
            this.colTipoSaida.Width = 57;
            // 
            // colValorTotal
            // 
            this.colValorTotal.Caption = "Valor Total";
            this.colValorTotal.DisplayFormat.FormatString = "0.00";
            this.colValorTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorTotal.FieldName = "ValorTotal";
            this.colValorTotal.MinWidth = 17;
            this.colValorTotal.Name = "colValorTotal";
            this.colValorTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorTotal", "Total = {0:c2}")});
            this.colValorTotal.Width = 113;
            // 
            // mEDICAMENTOSVENDIDOSBindingSource
            // 
            this.mEDICAMENTOSVENDIDOSBindingSource.DataSource = typeof(MegaReporter.Models.Entities.MEDICAMENTOS);
            // 
            // cOMBUSTIVEISVENDIDOSBindingSource
            // 
            this.cOMBUSTIVEISVENDIDOSBindingSource.DataSource = typeof(MegaReporter.Models.Entities.COMBUSTIVEIS);
            // 
            // gridControlPostos
            // 
            this.gridControlPostos.DataSource = this.cOMBUSTIVEISVENDIDOSBindingSource;
            this.gridControlPostos.Location = new System.Drawing.Point(695, 168);
            this.gridControlPostos.MainView = this.gridViewPostos;
            this.gridControlPostos.Name = "gridControlPostos";
            this.gridControlPostos.Size = new System.Drawing.Size(339, 156);
            this.gridControlPostos.TabIndex = 0;
            this.gridControlPostos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPostos});
            this.gridControlPostos.Visible = false;
            // 
            // gridViewPostos
            // 
            this.gridViewPostos.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewPostos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPostos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPostos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPostos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDataEmissao,
            this.colCliente,
            this.colDescricao1,
            this.colQuantidade,
            this.colPrecoVenda1,
            this.colDescricaoPlano1,
            this.colNomeVendedor4,
            this.colUnidade1,
            this.colDescontoSubtotal,
            this.colBico,
            this.colBomba,
            this.colTanque,
            this.colDescricaoTurno,
            this.colVeiculoPlaca,
            this.colVeiculoHodometro,
            this.colVinculada,
            this.colDataVinculamento,
            this.colNumero,
            this.colHora});
            this.gridViewPostos.GridControl = this.gridControlPostos;
            this.gridViewPostos.Name = "gridViewPostos";
            this.gridViewPostos.OptionsBehavior.Editable = false;
            this.gridViewPostos.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPostos.OptionsView.ShowFooter = true;
            // 
            // colDataEmissao
            // 
            this.colDataEmissao.FieldName = "DataEmissao";
            this.colDataEmissao.Name = "colDataEmissao";
            // 
            // colCliente
            // 
            this.colCliente.FieldName = "Cliente";
            this.colCliente.Name = "colCliente";
            // 
            // colDescricao1
            // 
            this.colDescricao1.FieldName = "Descricao";
            this.colDescricao1.Name = "colDescricao1";
            // 
            // colQuantidade
            // 
            this.colQuantidade.FieldName = "Quantidade";
            this.colQuantidade.Name = "colQuantidade";
            // 
            // colPrecoVenda1
            // 
            this.colPrecoVenda1.FieldName = "PrecoVenda";
            this.colPrecoVenda1.Name = "colPrecoVenda1";
            // 
            // colDescricaoPlano1
            // 
            this.colDescricaoPlano1.FieldName = "DescricaoPlano";
            this.colDescricaoPlano1.Name = "colDescricaoPlano1";
            // 
            // colNomeVendedor4
            // 
            this.colNomeVendedor4.FieldName = "NomeVendedor";
            this.colNomeVendedor4.Name = "colNomeVendedor4";
            // 
            // colUnidade1
            // 
            this.colUnidade1.FieldName = "Unidade";
            this.colUnidade1.Name = "colUnidade1";
            // 
            // colDescontoSubtotal
            // 
            this.colDescontoSubtotal.FieldName = "DescontoSubtotal";
            this.colDescontoSubtotal.Name = "colDescontoSubtotal";
            // 
            // colBico
            // 
            this.colBico.FieldName = "Bico";
            this.colBico.Name = "colBico";
            // 
            // colBomba
            // 
            this.colBomba.FieldName = "Bomba";
            this.colBomba.Name = "colBomba";
            // 
            // colTanque
            // 
            this.colTanque.FieldName = "Tanque";
            this.colTanque.Name = "colTanque";
            // 
            // colDescricaoTurno
            // 
            this.colDescricaoTurno.FieldName = "DescricaoTurno";
            this.colDescricaoTurno.Name = "colDescricaoTurno";
            // 
            // colVeiculoPlaca
            // 
            this.colVeiculoPlaca.FieldName = "VeiculoPlaca";
            this.colVeiculoPlaca.Name = "colVeiculoPlaca";
            // 
            // colVeiculoHodometro
            // 
            this.colVeiculoHodometro.FieldName = "VeiculoHodometro";
            this.colVeiculoHodometro.Name = "colVeiculoHodometro";
            // 
            // colVinculada
            // 
            this.colVinculada.FieldName = "Vinculada";
            this.colVinculada.Name = "colVinculada";
            // 
            // colDataVinculamento
            // 
            this.colDataVinculamento.FieldName = "DataVinculamento";
            this.colDataVinculamento.Name = "colDataVinculamento";
            // 
            // colNumero
            // 
            this.colNumero.FieldName = "Numero";
            this.colNumero.Name = "colNumero";
            // 
            // colHora
            // 
            this.colHora.FieldName = "Hora";
            this.colHora.Name = "colHora";
            // 
            // gridViewPadrao3
            // 
            this.gridViewPadrao3.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewPadrao3.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPadrao3.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPadrao3.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPadrao3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodigo6,
            this.colDataVenda5,
            this.colCliente6,
            this.colDescricao7,
            this.colCst4,
            this.colNomeVendedor6,
            this.colNcm4,
            this.colUnidade6,
            this.colDescricaoPlano7,
            this.colQuantidadeVendida6,
            this.colPrecoVenda7,
            this.colTipoSaida6,
            this.colCfop5,
            this.colBaseCalculo,
            this.colTotalLiquido5,
            this.colTotalBruto4,
            this.colTotalDesconto4,
            this.colPrecoBruto5,
            this.colReferencia5,
            this.colMarca5,
            this.colGrupo5,
            this.colOperacao4,
            this.colRazaoSocial4,
            this.colValorBaseIcms4,
            this.colValorAliquota4});
            this.gridViewPadrao3.GridControl = this.gridControlPadrao3;
            this.gridViewPadrao3.Name = "gridViewPadrao3";
            this.gridViewPadrao3.OptionsBehavior.Editable = false;
            this.gridViewPadrao3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPadrao3.OptionsView.ShowFooter = true;
            // 
            // colCodigo6
            // 
            this.colCodigo6.Caption = "Código";
            this.colCodigo6.FieldName = "Codigo";
            this.colCodigo6.Name = "colCodigo6";
            this.colCodigo6.Visible = true;
            this.colCodigo6.VisibleIndex = 0;
            // 
            // colDataVenda5
            // 
            this.colDataVenda5.Caption = "Data";
            this.colDataVenda5.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colDataVenda5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDataVenda5.FieldName = "DataVenda";
            this.colDataVenda5.Name = "colDataVenda5";
            this.colDataVenda5.Visible = true;
            this.colDataVenda5.VisibleIndex = 1;
            // 
            // colCliente6
            // 
            this.colCliente6.Caption = "Cliente";
            this.colCliente6.FieldName = "Cliente";
            this.colCliente6.Name = "colCliente6";
            this.colCliente6.Visible = true;
            this.colCliente6.VisibleIndex = 2;
            // 
            // colDescricao7
            // 
            this.colDescricao7.Caption = "Descrição";
            this.colDescricao7.FieldName = "Descricao";
            this.colDescricao7.Name = "colDescricao7";
            this.colDescricao7.Visible = true;
            this.colDescricao7.VisibleIndex = 3;
            // 
            // colCst4
            // 
            this.colCst4.Caption = "Cst";
            this.colCst4.FieldName = "Cst";
            this.colCst4.Name = "colCst4";
            this.colCst4.Visible = true;
            this.colCst4.VisibleIndex = 4;
            // 
            // colNomeVendedor6
            // 
            this.colNomeVendedor6.Caption = "Vendedor";
            this.colNomeVendedor6.FieldName = "NomeVendedor";
            this.colNomeVendedor6.Name = "colNomeVendedor6";
            this.colNomeVendedor6.Visible = true;
            this.colNomeVendedor6.VisibleIndex = 5;
            // 
            // colNcm4
            // 
            this.colNcm4.Caption = "Ncm";
            this.colNcm4.FieldName = "Ncm";
            this.colNcm4.Name = "colNcm4";
            this.colNcm4.Visible = true;
            this.colNcm4.VisibleIndex = 6;
            // 
            // colUnidade6
            // 
            this.colUnidade6.Caption = "UN";
            this.colUnidade6.FieldName = "Unidade";
            this.colUnidade6.Name = "colUnidade6";
            this.colUnidade6.Visible = true;
            this.colUnidade6.VisibleIndex = 7;
            // 
            // colDescricaoPlano7
            // 
            this.colDescricaoPlano7.Caption = "Plano de Vendas";
            this.colDescricaoPlano7.FieldName = "DescricaoPlano";
            this.colDescricaoPlano7.Name = "colDescricaoPlano7";
            this.colDescricaoPlano7.Visible = true;
            this.colDescricaoPlano7.VisibleIndex = 8;
            // 
            // colQuantidadeVendida6
            // 
            this.colQuantidadeVendida6.Caption = "Qtde";
            this.colQuantidadeVendida6.DisplayFormat.FormatString = "0.0000";
            this.colQuantidadeVendida6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida6.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida6.Name = "colQuantidadeVendida6";
            this.colQuantidadeVendida6.Visible = true;
            this.colQuantidadeVendida6.VisibleIndex = 9;
            // 
            // colPrecoVenda7
            // 
            this.colPrecoVenda7.Caption = "Pço Venda";
            this.colPrecoVenda7.DisplayFormat.FormatString = "0.0000";
            this.colPrecoVenda7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoVenda7.FieldName = "PrecoVenda";
            this.colPrecoVenda7.Name = "colPrecoVenda7";
            this.colPrecoVenda7.Visible = true;
            this.colPrecoVenda7.VisibleIndex = 10;
            // 
            // colTipoSaida6
            // 
            this.colTipoSaida6.Caption = "Tipo Saída";
            this.colTipoSaida6.FieldName = "TipoSaida";
            this.colTipoSaida6.Name = "colTipoSaida6";
            this.colTipoSaida6.Visible = true;
            this.colTipoSaida6.VisibleIndex = 11;
            // 
            // colCfop5
            // 
            this.colCfop5.Caption = "Cfop";
            this.colCfop5.FieldName = "Cfop";
            this.colCfop5.Name = "colCfop5";
            this.colCfop5.Visible = true;
            this.colCfop5.VisibleIndex = 12;
            // 
            // colBaseCalculo
            // 
            this.colBaseCalculo.Caption = "Vlr Base";
            this.colBaseCalculo.DisplayFormat.FormatString = "0.0000";
            this.colBaseCalculo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBaseCalculo.FieldName = "BaseCalculo";
            this.colBaseCalculo.Name = "colBaseCalculo";
            this.colBaseCalculo.Visible = true;
            this.colBaseCalculo.VisibleIndex = 13;
            // 
            // colTotalLiquido5
            // 
            this.colTotalLiquido5.Caption = "Total Líquido";
            this.colTotalLiquido5.DisplayFormat.FormatString = "0.00";
            this.colTotalLiquido5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalLiquido5.FieldName = "TotalLiquido";
            this.colTotalLiquido5.Name = "colTotalLiquido5";
            this.colTotalLiquido5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", "{0:n}")});
            this.colTotalLiquido5.Visible = true;
            this.colTotalLiquido5.VisibleIndex = 14;
            // 
            // colTotalBruto4
            // 
            this.colTotalBruto4.Caption = "Total Bruto";
            this.colTotalBruto4.DisplayFormat.FormatString = "0.0000";
            this.colTotalBruto4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalBruto4.FieldName = "TotalBruto";
            this.colTotalBruto4.Name = "colTotalBruto4";
            this.colTotalBruto4.Visible = true;
            this.colTotalBruto4.VisibleIndex = 15;
            // 
            // colTotalDesconto4
            // 
            this.colTotalDesconto4.Caption = "Desc. Total";
            this.colTotalDesconto4.DisplayFormat.FormatString = "0.0000";
            this.colTotalDesconto4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalDesconto4.FieldName = "TotalDesconto";
            this.colTotalDesconto4.Name = "colTotalDesconto4";
            this.colTotalDesconto4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalDesconto", "{0:n}")});
            this.colTotalDesconto4.Visible = true;
            this.colTotalDesconto4.VisibleIndex = 16;
            // 
            // colPrecoBruto5
            // 
            this.colPrecoBruto5.Caption = "Pço Bruto";
            this.colPrecoBruto5.DisplayFormat.FormatString = "0.0000";
            this.colPrecoBruto5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoBruto5.FieldName = "PrecoBruto";
            this.colPrecoBruto5.Name = "colPrecoBruto5";
            this.colPrecoBruto5.Visible = true;
            this.colPrecoBruto5.VisibleIndex = 17;
            // 
            // colReferencia5
            // 
            this.colReferencia5.Caption = "Referência";
            this.colReferencia5.FieldName = "Referencia";
            this.colReferencia5.Name = "colReferencia5";
            this.colReferencia5.Visible = true;
            this.colReferencia5.VisibleIndex = 18;
            // 
            // colMarca5
            // 
            this.colMarca5.Caption = "Marca";
            this.colMarca5.FieldName = "Marca";
            this.colMarca5.Name = "colMarca5";
            this.colMarca5.Visible = true;
            this.colMarca5.VisibleIndex = 19;
            // 
            // colGrupo5
            // 
            this.colGrupo5.Caption = "Grupo";
            this.colGrupo5.FieldName = "Grupo";
            this.colGrupo5.Name = "colGrupo5";
            this.colGrupo5.Visible = true;
            this.colGrupo5.VisibleIndex = 20;
            // 
            // colOperacao4
            // 
            this.colOperacao4.Caption = "Operação";
            this.colOperacao4.FieldName = "Operacao";
            this.colOperacao4.Name = "colOperacao4";
            this.colOperacao4.Visible = true;
            this.colOperacao4.VisibleIndex = 21;
            // 
            // colRazaoSocial4
            // 
            this.colRazaoSocial4.Caption = "Razão Social";
            this.colRazaoSocial4.FieldName = "RazaoSocial";
            this.colRazaoSocial4.Name = "colRazaoSocial4";
            this.colRazaoSocial4.Visible = true;
            this.colRazaoSocial4.VisibleIndex = 22;
            // 
            // colValorBaseIcms4
            // 
            this.colValorBaseIcms4.Caption = "Valor Icms";
            this.colValorBaseIcms4.DisplayFormat.FormatString = "0.0000";
            this.colValorBaseIcms4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorBaseIcms4.FieldName = "ValorBaseIcms";
            this.colValorBaseIcms4.Name = "colValorBaseIcms4";
            this.colValorBaseIcms4.OptionsColumn.ReadOnly = true;
            this.colValorBaseIcms4.Visible = true;
            this.colValorBaseIcms4.VisibleIndex = 23;
            // 
            // colValorAliquota4
            // 
            this.colValorAliquota4.Caption = "Valor Alíquota";
            this.colValorAliquota4.DisplayFormat.FormatString = "0.0000";
            this.colValorAliquota4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAliquota4.FieldName = "ValorAliquota";
            this.colValorAliquota4.Name = "colValorAliquota4";
            this.colValorAliquota4.OptionsColumn.ReadOnly = true;
            this.colValorAliquota4.Visible = true;
            this.colValorAliquota4.VisibleIndex = 24;
            // 
            // gridControlPadrao3
            // 
            this.gridControlPadrao3.DataSource = this.pRODUTOSVENDIDOSPORPLANOBindingSource;
            this.gridControlPadrao3.Location = new System.Drawing.Point(449, 345);
            this.gridControlPadrao3.MainView = this.gridViewPadrao3;
            this.gridControlPadrao3.Name = "gridControlPadrao3";
            this.gridControlPadrao3.Size = new System.Drawing.Size(326, 151);
            this.gridControlPadrao3.TabIndex = 0;
            this.gridControlPadrao3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPadrao3});
            this.gridControlPadrao3.Visible = false;
            // 
            // gridControlMedicamentos
            // 
            this.gridControlMedicamentos.DataSource = this.mEDICAMENTOSVENDIDOSBindingSource;
            this.gridControlMedicamentos.Location = new System.Drawing.Point(309, 112);
            this.gridControlMedicamentos.MainView = this.gridViewMedicamentos;
            this.gridControlMedicamentos.Name = "gridControlMedicamentos";
            this.gridControlMedicamentos.Size = new System.Drawing.Size(296, 178);
            this.gridControlMedicamentos.TabIndex = 0;
            this.gridControlMedicamentos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMedicamentos});
            this.gridControlMedicamentos.Visible = false;
            // 
            // gridViewMedicamentos
            // 
            this.gridViewMedicamentos.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewMedicamentos.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewMedicamentos.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewMedicamentos.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewMedicamentos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDataVenda1,
            this.colNotaFiscal,
            this.colCodigo5,
            this.colDescricao6,
            this.colCfop4,
            this.colQuantidadeVendida2,
            this.colLote3,
            this.colFabricacao3,
            this.colVencimento3,
            this.colPrecoVenda6,
            this.colDesconto,
            this.colDescricaoPlano6,
            this.colCliente5,
            this.colNomeVendedor5,
            this.colMarca4,
            this.colGrupo4,
            this.colRazaoSocial3,
            this.colTipoSaida2,
            this.colPrecoCusto,
            this.colPrecoBruto1,
            this.colTotalLiquido4,
            this.colOperacao5,
            this.colReferencia4});
            this.gridViewMedicamentos.GridControl = this.gridControlMedicamentos;
            this.gridViewMedicamentos.Name = "gridViewMedicamentos";
            this.gridViewMedicamentos.OptionsBehavior.Editable = false;
            this.gridViewMedicamentos.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewMedicamentos.OptionsView.ShowFooter = true;
            // 
            // colDataVenda1
            // 
            this.colDataVenda1.Caption = "Data";
            this.colDataVenda1.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colDataVenda1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDataVenda1.FieldName = "DataVenda";
            this.colDataVenda1.Name = "colDataVenda1";
            this.colDataVenda1.Visible = true;
            this.colDataVenda1.VisibleIndex = 0;
            // 
            // colNotaFiscal
            // 
            this.colNotaFiscal.Caption = "Nota Fiscal";
            this.colNotaFiscal.FieldName = "NotaFiscal";
            this.colNotaFiscal.Name = "colNotaFiscal";
            this.colNotaFiscal.Visible = true;
            this.colNotaFiscal.VisibleIndex = 1;
            // 
            // colCodigo5
            // 
            this.colCodigo5.Caption = "Código";
            this.colCodigo5.FieldName = "Codigo";
            this.colCodigo5.Name = "colCodigo5";
            this.colCodigo5.Visible = true;
            this.colCodigo5.VisibleIndex = 2;
            // 
            // colDescricao6
            // 
            this.colDescricao6.Caption = "Descrição";
            this.colDescricao6.FieldName = "Descricao";
            this.colDescricao6.Name = "colDescricao6";
            this.colDescricao6.Visible = true;
            this.colDescricao6.VisibleIndex = 3;
            // 
            // colCfop4
            // 
            this.colCfop4.Caption = "Cfop";
            this.colCfop4.FieldName = "Cfop";
            this.colCfop4.Name = "colCfop4";
            this.colCfop4.Visible = true;
            this.colCfop4.VisibleIndex = 4;
            // 
            // colQuantidadeVendida2
            // 
            this.colQuantidadeVendida2.Caption = "Qtde";
            this.colQuantidadeVendida2.DisplayFormat.FormatString = "0.0000";
            this.colQuantidadeVendida2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida2.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida2.Name = "colQuantidadeVendida2";
            this.colQuantidadeVendida2.Visible = true;
            this.colQuantidadeVendida2.VisibleIndex = 5;
            // 
            // colLote3
            // 
            this.colLote3.Caption = "Lote";
            this.colLote3.FieldName = "Lote";
            this.colLote3.Name = "colLote3";
            this.colLote3.Visible = true;
            this.colLote3.VisibleIndex = 6;
            // 
            // colFabricacao3
            // 
            this.colFabricacao3.Caption = "Fabricação";
            this.colFabricacao3.CustomizationCaption = "dd/MM/yyyy";
            this.colFabricacao3.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colFabricacao3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFabricacao3.FieldName = "Fabricacao";
            this.colFabricacao3.GroupFormat.FormatString = "dd/MM/yyyy";
            this.colFabricacao3.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colFabricacao3.Name = "colFabricacao3";
            this.colFabricacao3.Visible = true;
            this.colFabricacao3.VisibleIndex = 7;
            // 
            // colVencimento3
            // 
            this.colVencimento3.Caption = "Vencimento";
            this.colVencimento3.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colVencimento3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colVencimento3.FieldName = "Vencimento";
            this.colVencimento3.Name = "colVencimento3";
            this.colVencimento3.Visible = true;
            this.colVencimento3.VisibleIndex = 8;
            // 
            // colPrecoVenda6
            // 
            this.colPrecoVenda6.Caption = "Pço Venda";
            this.colPrecoVenda6.DisplayFormat.FormatString = "0.0000";
            this.colPrecoVenda6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoVenda6.FieldName = "PrecoVenda";
            this.colPrecoVenda6.Name = "colPrecoVenda6";
            this.colPrecoVenda6.Visible = true;
            this.colPrecoVenda6.VisibleIndex = 9;
            // 
            // colDesconto
            // 
            this.colDesconto.Caption = "Desconto";
            this.colDesconto.DisplayFormat.FormatString = "0.0000";
            this.colDesconto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDesconto.FieldName = "Desconto";
            this.colDesconto.Name = "colDesconto";
            this.colDesconto.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Desconto", "{0:n}")});
            this.colDesconto.Visible = true;
            this.colDesconto.VisibleIndex = 10;
            // 
            // colDescricaoPlano6
            // 
            this.colDescricaoPlano6.Caption = "Plano de Vendas";
            this.colDescricaoPlano6.FieldName = "DescricaoPlano";
            this.colDescricaoPlano6.Name = "colDescricaoPlano6";
            this.colDescricaoPlano6.Visible = true;
            this.colDescricaoPlano6.VisibleIndex = 11;
            // 
            // colCliente5
            // 
            this.colCliente5.Caption = "Cliente";
            this.colCliente5.FieldName = "Cliente";
            this.colCliente5.Name = "colCliente5";
            this.colCliente5.Visible = true;
            this.colCliente5.VisibleIndex = 12;
            // 
            // colNomeVendedor5
            // 
            this.colNomeVendedor5.Caption = "Vendedor";
            this.colNomeVendedor5.FieldName = "NomeVendedor";
            this.colNomeVendedor5.Name = "colNomeVendedor5";
            this.colNomeVendedor5.Visible = true;
            this.colNomeVendedor5.VisibleIndex = 13;
            // 
            // colMarca4
            // 
            this.colMarca4.Caption = "Marca";
            this.colMarca4.FieldName = "Marca";
            this.colMarca4.Name = "colMarca4";
            this.colMarca4.Visible = true;
            this.colMarca4.VisibleIndex = 14;
            // 
            // colGrupo4
            // 
            this.colGrupo4.Caption = "Grupo";
            this.colGrupo4.FieldName = "Grupo";
            this.colGrupo4.Name = "colGrupo4";
            this.colGrupo4.Visible = true;
            this.colGrupo4.VisibleIndex = 15;
            // 
            // colRazaoSocial3
            // 
            this.colRazaoSocial3.Caption = "Razão Social";
            this.colRazaoSocial3.FieldName = "RazaoSocial";
            this.colRazaoSocial3.Name = "colRazaoSocial3";
            this.colRazaoSocial3.Visible = true;
            this.colRazaoSocial3.VisibleIndex = 16;
            // 
            // colTipoSaida2
            // 
            this.colTipoSaida2.Caption = "Tipo Saída";
            this.colTipoSaida2.FieldName = "TipoSaida";
            this.colTipoSaida2.Name = "colTipoSaida2";
            this.colTipoSaida2.Visible = true;
            this.colTipoSaida2.VisibleIndex = 17;
            // 
            // colPrecoCusto
            // 
            this.colPrecoCusto.Caption = "Pço Custo";
            this.colPrecoCusto.DisplayFormat.FormatString = "0.0000";
            this.colPrecoCusto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoCusto.FieldName = "PrecoCusto";
            this.colPrecoCusto.Name = "colPrecoCusto";
            this.colPrecoCusto.Visible = true;
            this.colPrecoCusto.VisibleIndex = 18;
            // 
            // colPrecoBruto1
            // 
            this.colPrecoBruto1.Caption = "Pço Bruto";
            this.colPrecoBruto1.DisplayFormat.FormatString = "0.0000";
            this.colPrecoBruto1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoBruto1.FieldName = "PrecoBruto";
            this.colPrecoBruto1.Name = "colPrecoBruto1";
            this.colPrecoBruto1.Visible = true;
            this.colPrecoBruto1.VisibleIndex = 19;
            // 
            // colTotalLiquido4
            // 
            this.colTotalLiquido4.Caption = "Total Líquido";
            this.colTotalLiquido4.DisplayFormat.FormatString = "0.00";
            this.colTotalLiquido4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalLiquido4.FieldName = "TotalLiquido";
            this.colTotalLiquido4.Name = "colTotalLiquido4";
            this.colTotalLiquido4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", "{0:n}")});
            this.colTotalLiquido4.Visible = true;
            this.colTotalLiquido4.VisibleIndex = 20;
            // 
            // colOperacao5
            // 
            this.colOperacao5.FieldName = "Operacao";
            this.colOperacao5.Name = "colOperacao5";
            this.colOperacao5.Visible = true;
            this.colOperacao5.VisibleIndex = 21;
            // 
            // colReferencia4
            // 
            this.colReferencia4.Caption = "Referência";
            this.colReferencia4.FieldName = "Referencia";
            this.colReferencia4.Name = "colReferencia4";
            this.colReferencia4.Visible = true;
            this.colReferencia4.VisibleIndex = 22;
            // 
            // gridViewPadrao1
            // 
            this.gridViewPadrao1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewPadrao1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPadrao1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPadrao1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPadrao1.Appearance.Row.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.gridViewPadrao1.AppearancePrint.Preview.BackColor = System.Drawing.Color.White;
            this.gridViewPadrao1.AppearancePrint.Preview.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.gridViewPadrao1.AppearancePrint.Preview.Options.UseBackColor = true;
            this.gridViewPadrao1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodigo1,
            this.colDataVenda,
            this.colCliente1,
            this.colDescricao2,
            this.colCst,
            this.colNomeVendedor,
            this.colNcm,
            this.colUnidade2,
            this.colDescricaoPlano2,
            this.colQuantidadeVendida1,
            this.colPrecoVenda2,
            this.colTipoSaida1,
            this.colCfop,
            this.colTotalLiquido,
            this.colTotalBruto3,
            this.colTotalDesconto1,
            this.colPrecoBruto,
            this.colReferencia3,
            this.colMarca3,
            this.colGrupo3,
            this.colOperacao,
            this.colRazaoSocial1,
            this.colValorBaseIcms3,
            this.colValorAliquota3});
            this.gridViewPadrao1.GridControl = this.gridControlPadrao1;
            this.gridViewPadrao1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", this.colTotalLiquido, "Total: {0:n}")});
            this.gridViewPadrao1.Name = "gridViewPadrao1";
            this.gridViewPadrao1.OptionsBehavior.Editable = false;
            this.gridViewPadrao1.OptionsMenu.ShowAddNewSummaryItem = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewPadrao1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewPadrao1.OptionsMenu.ShowSummaryItemMode = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewPadrao1.OptionsPrint.RtfPageFooter = resources.GetString("gridViewPadrao1.OptionsPrint.RtfPageFooter");
            this.gridViewPadrao1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPadrao1.OptionsView.ColumnAutoWidth = false;
            this.gridViewPadrao1.OptionsView.ShowFooter = true;
            // 
            // colCodigo1
            // 
            this.colCodigo1.Caption = "Código";
            this.colCodigo1.FieldName = "Codigo";
            this.colCodigo1.Name = "colCodigo1";
            this.colCodigo1.Visible = true;
            this.colCodigo1.VisibleIndex = 0;
            // 
            // colDataVenda
            // 
            this.colDataVenda.Caption = "Data";
            this.colDataVenda.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colDataVenda.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDataVenda.FieldName = "DataVenda";
            this.colDataVenda.GroupFormat.FormatString = "dd/MM/yyyy";
            this.colDataVenda.Name = "colDataVenda";
            this.colDataVenda.Visible = true;
            this.colDataVenda.VisibleIndex = 1;
            // 
            // colCliente1
            // 
            this.colCliente1.Caption = "Cliente";
            this.colCliente1.FieldName = "Cliente";
            this.colCliente1.Name = "colCliente1";
            this.colCliente1.Visible = true;
            this.colCliente1.VisibleIndex = 2;
            // 
            // colDescricao2
            // 
            this.colDescricao2.Caption = "Descrição";
            this.colDescricao2.FieldName = "Descricao";
            this.colDescricao2.Name = "colDescricao2";
            this.colDescricao2.Visible = true;
            this.colDescricao2.VisibleIndex = 3;
            // 
            // colCst
            // 
            this.colCst.Caption = "Cst";
            this.colCst.FieldName = "Cst";
            this.colCst.Name = "colCst";
            this.colCst.Visible = true;
            this.colCst.VisibleIndex = 4;
            // 
            // colNomeVendedor
            // 
            this.colNomeVendedor.Caption = "Vendedor";
            this.colNomeVendedor.FieldName = "NomeVendedor";
            this.colNomeVendedor.Name = "colNomeVendedor";
            this.colNomeVendedor.Visible = true;
            this.colNomeVendedor.VisibleIndex = 5;
            // 
            // colNcm
            // 
            this.colNcm.Caption = "Ncm";
            this.colNcm.FieldName = "Ncm";
            this.colNcm.Name = "colNcm";
            this.colNcm.Visible = true;
            this.colNcm.VisibleIndex = 6;
            // 
            // colUnidade2
            // 
            this.colUnidade2.Caption = "UN";
            this.colUnidade2.FieldName = "Unidade";
            this.colUnidade2.Name = "colUnidade2";
            this.colUnidade2.Visible = true;
            this.colUnidade2.VisibleIndex = 7;
            // 
            // colDescricaoPlano2
            // 
            this.colDescricaoPlano2.Caption = "Plano de Vendas";
            this.colDescricaoPlano2.FieldName = "DescricaoPlano";
            this.colDescricaoPlano2.Name = "colDescricaoPlano2";
            this.colDescricaoPlano2.Visible = true;
            this.colDescricaoPlano2.VisibleIndex = 8;
            // 
            // colQuantidadeVendida1
            // 
            this.colQuantidadeVendida1.Caption = "Qtde";
            this.colQuantidadeVendida1.DisplayFormat.FormatString = "0.0000";
            this.colQuantidadeVendida1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida1.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida1.Name = "colQuantidadeVendida1";
            this.colQuantidadeVendida1.Visible = true;
            this.colQuantidadeVendida1.VisibleIndex = 9;
            // 
            // colPrecoVenda2
            // 
            this.colPrecoVenda2.Caption = "Pço Venda";
            this.colPrecoVenda2.DisplayFormat.FormatString = "0.0000";
            this.colPrecoVenda2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoVenda2.FieldName = "PrecoVenda";
            this.colPrecoVenda2.Name = "colPrecoVenda2";
            this.colPrecoVenda2.Visible = true;
            this.colPrecoVenda2.VisibleIndex = 10;
            // 
            // colTipoSaida1
            // 
            this.colTipoSaida1.Caption = "Tipo Saída";
            this.colTipoSaida1.FieldName = "TipoSaida";
            this.colTipoSaida1.Name = "colTipoSaida1";
            this.colTipoSaida1.Visible = true;
            this.colTipoSaida1.VisibleIndex = 11;
            // 
            // colCfop
            // 
            this.colCfop.Caption = "Cfop";
            this.colCfop.FieldName = "Cfop";
            this.colCfop.Name = "colCfop";
            this.colCfop.Visible = true;
            this.colCfop.VisibleIndex = 12;
            // 
            // colTotalLiquido
            // 
            this.colTotalLiquido.Caption = "Total Líquido";
            this.colTotalLiquido.DisplayFormat.FormatString = "0.00";
            this.colTotalLiquido.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalLiquido.FieldName = "TotalLiquido";
            this.colTotalLiquido.Name = "colTotalLiquido";
            this.colTotalLiquido.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", "{0:n}")});
            this.colTotalLiquido.Visible = true;
            this.colTotalLiquido.VisibleIndex = 13;
            // 
            // colTotalBruto3
            // 
            this.colTotalBruto3.Caption = "Total Bruto";
            this.colTotalBruto3.DisplayFormat.FormatString = "0.0000";
            this.colTotalBruto3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalBruto3.FieldName = "TotalBruto";
            this.colTotalBruto3.Name = "colTotalBruto3";
            this.colTotalBruto3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalBruto", "{0:n}")});
            this.colTotalBruto3.Visible = true;
            this.colTotalBruto3.VisibleIndex = 14;
            // 
            // colTotalDesconto1
            // 
            this.colTotalDesconto1.Caption = "  Desc. Total   ";
            this.colTotalDesconto1.DisplayFormat.FormatString = "0.0000";
            this.colTotalDesconto1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalDesconto1.FieldName = "TotalDesconto";
            this.colTotalDesconto1.Name = "colTotalDesconto1";
            this.colTotalDesconto1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalDesconto", "{0:n}")});
            this.colTotalDesconto1.Visible = true;
            this.colTotalDesconto1.VisibleIndex = 15;
            // 
            // colPrecoBruto
            // 
            this.colPrecoBruto.Caption = "Pço Bruto";
            this.colPrecoBruto.DisplayFormat.FormatString = "0.0000";
            this.colPrecoBruto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoBruto.FieldName = "PrecoBruto";
            this.colPrecoBruto.Name = "colPrecoBruto";
            this.colPrecoBruto.Visible = true;
            this.colPrecoBruto.VisibleIndex = 16;
            // 
            // colReferencia3
            // 
            this.colReferencia3.Caption = "Referência";
            this.colReferencia3.FieldName = "Referencia";
            this.colReferencia3.Name = "colReferencia3";
            this.colReferencia3.Visible = true;
            this.colReferencia3.VisibleIndex = 17;
            // 
            // colMarca3
            // 
            this.colMarca3.Caption = "Marca";
            this.colMarca3.FieldName = "Marca";
            this.colMarca3.Name = "colMarca3";
            this.colMarca3.Visible = true;
            this.colMarca3.VisibleIndex = 18;
            // 
            // colGrupo3
            // 
            this.colGrupo3.Caption = "Grupo";
            this.colGrupo3.FieldName = "Grupo";
            this.colGrupo3.Name = "colGrupo3";
            this.colGrupo3.Visible = true;
            this.colGrupo3.VisibleIndex = 19;
            // 
            // colOperacao
            // 
            this.colOperacao.Caption = "Operação";
            this.colOperacao.FieldName = "Operacao";
            this.colOperacao.Name = "colOperacao";
            this.colOperacao.Visible = true;
            this.colOperacao.VisibleIndex = 20;
            // 
            // colRazaoSocial1
            // 
            this.colRazaoSocial1.Caption = "Razão Social";
            this.colRazaoSocial1.FieldName = "RazaoSocial";
            this.colRazaoSocial1.Name = "colRazaoSocial1";
            this.colRazaoSocial1.Visible = true;
            this.colRazaoSocial1.VisibleIndex = 21;
            // 
            // colValorBaseIcms3
            // 
            this.colValorBaseIcms3.Caption = "Valor Icms";
            this.colValorBaseIcms3.DisplayFormat.FormatString = "0.0000";
            this.colValorBaseIcms3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorBaseIcms3.FieldName = "ValorBaseIcms";
            this.colValorBaseIcms3.Name = "colValorBaseIcms3";
            this.colValorBaseIcms3.OptionsColumn.ReadOnly = true;
            this.colValorBaseIcms3.Visible = true;
            this.colValorBaseIcms3.VisibleIndex = 22;
            // 
            // colValorAliquota3
            // 
            this.colValorAliquota3.Caption = "Valor Alíquota";
            this.colValorAliquota3.DisplayFormat.FormatString = "0.0000";
            this.colValorAliquota3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAliquota3.FieldName = "ValorAliquota";
            this.colValorAliquota3.Name = "colValorAliquota3";
            this.colValorAliquota3.OptionsColumn.ReadOnly = true;
            this.colValorAliquota3.Visible = true;
            this.colValorAliquota3.VisibleIndex = 23;
            // 
            // gridControlPadrao1
            // 
            this.gridControlPadrao1.DataSource = this.pRODUTOSVENDIDOSPORPLANOBindingSource;
            this.gridControlPadrao1.Location = new System.Drawing.Point(33, 112);
            this.gridControlPadrao1.MainView = this.gridViewPadrao1;
            this.gridControlPadrao1.Name = "gridControlPadrao1";
            this.gridControlPadrao1.Size = new System.Drawing.Size(250, 157);
            this.gridControlPadrao1.TabIndex = 0;
            this.gridControlPadrao1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPadrao1,
            this.gridView10});
            this.gridControlPadrao1.Visible = false;
            this.gridControlPadrao1.Click += new System.EventHandler(this.gridControl2_Click);
            // 
            // gridView10
            // 
            this.gridView10.GridControl = this.gridControlPadrao1;
            this.gridView10.Name = "gridView10";
            // 
            // gridViewPadrao2
            // 
            this.gridViewPadrao2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewPadrao2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewPadrao2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewPadrao2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewPadrao2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodigo2,
            this.colDataVenda2,
            this.colCliente2,
            this.colDescricao3,
            this.colCst1,
            this.colNomeVendedor1,
            this.colNcm1,
            this.colUnidade3,
            this.colDescricaoPlano3,
            this.colQuantidadeVendida3,
            this.colPrecoVenda3,
            this.colTipoSaida3,
            this.colCfop1,
            this.colTotalLiquido1,
            this.colTotalBruto,
            this.colTotalDesconto,
            this.colPrecoBruto2,
            this.colReferencia,
            this.colMarca,
            this.colGrupo,
            this.colOperacao1,
            this.colValorBaseIcms,
            this.colRazaoSocial2,
            this.colValorAliquota});
            this.gridViewPadrao2.GridControl = this.gridControlPadrao2;
            this.gridViewPadrao2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewPadrao2.Name = "gridViewPadrao2";
            this.gridViewPadrao2.OptionsBehavior.Editable = false;
            this.gridViewPadrao2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewPadrao2.OptionsView.ShowFooter = true;
            // 
            // colCodigo2
            // 
            this.colCodigo2.Caption = "Código";
            this.colCodigo2.FieldName = "Codigo";
            this.colCodigo2.Name = "colCodigo2";
            this.colCodigo2.Visible = true;
            this.colCodigo2.VisibleIndex = 0;
            // 
            // colDataVenda2
            // 
            this.colDataVenda2.Caption = "Data";
            this.colDataVenda2.DisplayFormat.FormatString = "dd/MM/yyyy ddd";
            this.colDataVenda2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDataVenda2.FieldName = "DataVenda";
            this.colDataVenda2.Name = "colDataVenda2";
            this.colDataVenda2.Visible = true;
            this.colDataVenda2.VisibleIndex = 1;
            // 
            // colCliente2
            // 
            this.colCliente2.Caption = "Cliente";
            this.colCliente2.FieldName = "Cliente";
            this.colCliente2.Name = "colCliente2";
            this.colCliente2.Visible = true;
            this.colCliente2.VisibleIndex = 2;
            // 
            // colDescricao3
            // 
            this.colDescricao3.Caption = "Descrição";
            this.colDescricao3.FieldName = "Descricao";
            this.colDescricao3.Name = "colDescricao3";
            this.colDescricao3.Visible = true;
            this.colDescricao3.VisibleIndex = 3;
            // 
            // colCst1
            // 
            this.colCst1.Caption = "Cst";
            this.colCst1.FieldName = "Cst";
            this.colCst1.Name = "colCst1";
            this.colCst1.Visible = true;
            this.colCst1.VisibleIndex = 4;
            // 
            // colNomeVendedor1
            // 
            this.colNomeVendedor1.Caption = "Vendedor";
            this.colNomeVendedor1.FieldName = "NomeVendedor";
            this.colNomeVendedor1.Name = "colNomeVendedor1";
            this.colNomeVendedor1.Visible = true;
            this.colNomeVendedor1.VisibleIndex = 5;
            // 
            // colNcm1
            // 
            this.colNcm1.Caption = "Ncm";
            this.colNcm1.FieldName = "Ncm";
            this.colNcm1.Name = "colNcm1";
            this.colNcm1.Visible = true;
            this.colNcm1.VisibleIndex = 6;
            // 
            // colUnidade3
            // 
            this.colUnidade3.Caption = "UN";
            this.colUnidade3.FieldName = "Unidade";
            this.colUnidade3.Name = "colUnidade3";
            this.colUnidade3.Visible = true;
            this.colUnidade3.VisibleIndex = 7;
            // 
            // colDescricaoPlano3
            // 
            this.colDescricaoPlano3.Caption = "Plano de Vendas";
            this.colDescricaoPlano3.FieldName = "DescricaoPlano";
            this.colDescricaoPlano3.Name = "colDescricaoPlano3";
            this.colDescricaoPlano3.Visible = true;
            this.colDescricaoPlano3.VisibleIndex = 8;
            // 
            // colQuantidadeVendida3
            // 
            this.colQuantidadeVendida3.Caption = "Qtde";
            this.colQuantidadeVendida3.DisplayFormat.FormatString = "0.0000";
            this.colQuantidadeVendida3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantidadeVendida3.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida3.Name = "colQuantidadeVendida3";
            this.colQuantidadeVendida3.Visible = true;
            this.colQuantidadeVendida3.VisibleIndex = 9;
            // 
            // colPrecoVenda3
            // 
            this.colPrecoVenda3.Caption = "Pço Venda";
            this.colPrecoVenda3.DisplayFormat.FormatString = "0.0000";
            this.colPrecoVenda3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoVenda3.FieldName = "PrecoVenda";
            this.colPrecoVenda3.Name = "colPrecoVenda3";
            this.colPrecoVenda3.Visible = true;
            this.colPrecoVenda3.VisibleIndex = 10;
            // 
            // colTipoSaida3
            // 
            this.colTipoSaida3.Caption = "Tipo Saída";
            this.colTipoSaida3.FieldName = "TipoSaida";
            this.colTipoSaida3.Name = "colTipoSaida3";
            this.colTipoSaida3.Visible = true;
            this.colTipoSaida3.VisibleIndex = 11;
            // 
            // colCfop1
            // 
            this.colCfop1.Caption = "Cfop";
            this.colCfop1.FieldName = "Cfop";
            this.colCfop1.Name = "colCfop1";
            this.colCfop1.Visible = true;
            this.colCfop1.VisibleIndex = 12;
            // 
            // colTotalLiquido1
            // 
            this.colTotalLiquido1.Caption = "Total Líquido";
            this.colTotalLiquido1.DisplayFormat.FormatString = "0.00";
            this.colTotalLiquido1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalLiquido1.FieldName = "TotalLiquido";
            this.colTotalLiquido1.Name = "colTotalLiquido1";
            this.colTotalLiquido1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", "{0:n}")});
            this.colTotalLiquido1.Visible = true;
            this.colTotalLiquido1.VisibleIndex = 13;
            // 
            // colTotalBruto
            // 
            this.colTotalBruto.Caption = "Total Bruto";
            this.colTotalBruto.DisplayFormat.FormatString = "0.0000";
            this.colTotalBruto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalBruto.FieldName = "TotalBruto";
            this.colTotalBruto.Name = "colTotalBruto";
            this.colTotalBruto.Visible = true;
            this.colTotalBruto.VisibleIndex = 14;
            // 
            // colTotalDesconto
            // 
            this.colTotalDesconto.Caption = "Desc. Total";
            this.colTotalDesconto.DisplayFormat.FormatString = "0.0000";
            this.colTotalDesconto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalDesconto.FieldName = "TotalDesconto";
            this.colTotalDesconto.Name = "colTotalDesconto";
            this.colTotalDesconto.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalDesconto", "{0:n}")});
            this.colTotalDesconto.Visible = true;
            this.colTotalDesconto.VisibleIndex = 15;
            // 
            // colPrecoBruto2
            // 
            this.colPrecoBruto2.Caption = "Preço Bruto";
            this.colPrecoBruto2.DisplayFormat.FormatString = "0.0000";
            this.colPrecoBruto2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrecoBruto2.FieldName = "PrecoBruto";
            this.colPrecoBruto2.Name = "colPrecoBruto2";
            this.colPrecoBruto2.Visible = true;
            this.colPrecoBruto2.VisibleIndex = 16;
            // 
            // colReferencia
            // 
            this.colReferencia.Caption = "Referência";
            this.colReferencia.FieldName = "Referencia";
            this.colReferencia.Name = "colReferencia";
            this.colReferencia.Visible = true;
            this.colReferencia.VisibleIndex = 17;
            // 
            // colMarca
            // 
            this.colMarca.Caption = "Marca";
            this.colMarca.FieldName = "Marca";
            this.colMarca.Name = "colMarca";
            this.colMarca.Visible = true;
            this.colMarca.VisibleIndex = 18;
            // 
            // colGrupo
            // 
            this.colGrupo.Caption = "Grupo";
            this.colGrupo.FieldName = "Grupo";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.Visible = true;
            this.colGrupo.VisibleIndex = 19;
            // 
            // colOperacao1
            // 
            this.colOperacao1.Caption = "Operação";
            this.colOperacao1.FieldName = "Operacao";
            this.colOperacao1.Name = "colOperacao1";
            this.colOperacao1.Visible = true;
            this.colOperacao1.VisibleIndex = 20;
            // 
            // colValorBaseIcms
            // 
            this.colValorBaseIcms.Caption = "Valor Icms";
            this.colValorBaseIcms.DisplayFormat.FormatString = "0.0000";
            this.colValorBaseIcms.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorBaseIcms.FieldName = "ValorBaseIcms";
            this.colValorBaseIcms.Name = "colValorBaseIcms";
            this.colValorBaseIcms.OptionsColumn.ReadOnly = true;
            this.colValorBaseIcms.Visible = true;
            this.colValorBaseIcms.VisibleIndex = 21;
            // 
            // colRazaoSocial2
            // 
            this.colRazaoSocial2.Caption = "Razão Social";
            this.colRazaoSocial2.FieldName = "RazaoSocial";
            this.colRazaoSocial2.Name = "colRazaoSocial2";
            this.colRazaoSocial2.Visible = true;
            this.colRazaoSocial2.VisibleIndex = 22;
            // 
            // colValorAliquota
            // 
            this.colValorAliquota.Caption = "Valor Alíquota";
            this.colValorAliquota.DisplayFormat.FormatString = "0.0000";
            this.colValorAliquota.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colValorAliquota.FieldName = "ValorAliquota";
            this.colValorAliquota.Name = "colValorAliquota";
            this.colValorAliquota.OptionsColumn.ReadOnly = true;
            this.colValorAliquota.Visible = true;
            this.colValorAliquota.VisibleIndex = 23;
            // 
            // gridControlPadrao2
            // 
            this.gridControlPadrao2.DataSource = this.pRODUTOSVENDIDOSPORPLANOBindingSource;
            this.gridControlPadrao2.Location = new System.Drawing.Point(33, 296);
            this.gridControlPadrao2.MainView = this.gridViewPadrao2;
            this.gridControlPadrao2.Name = "gridControlPadrao2";
            this.gridControlPadrao2.Size = new System.Drawing.Size(320, 168);
            this.gridControlPadrao2.TabIndex = 0;
            this.gridControlPadrao2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPadrao2,
            this.gridView9});
            this.gridControlPadrao2.Visible = false;
            // 
            // gridView9
            // 
            this.gridView9.GridControl = this.gridControlPadrao2;
            this.gridView9.Name = "gridView9";
            // 
            // gridView8
            // 
            this.gridView8.Name = "gridView8";
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 560);
            this.panel12.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(1290, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(10, 560);
            this.panel13.TabIndex = 14;
            // 
            // panel14
            // 
            this.panel14.Location = new System.Drawing.Point(1290, 88);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 482);
            this.panel14.TabIndex = 15;
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel15.Location = new System.Drawing.Point(0, 560);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1300, 10);
            this.panel15.TabIndex = 15;
            // 
            // gridControlGenêrico
            // 
            this.gridControlGenêrico.DataSource = this.pRODUTOSVENDIDOSPORPLANOBindingSource;
            this.gridControlGenêrico.Location = new System.Drawing.Point(695, 274);
            this.gridControlGenêrico.MainView = this.gridViewGenêrico;
            this.gridControlGenêrico.Name = "gridControlGenêrico";
            this.gridControlGenêrico.Size = new System.Drawing.Size(334, 178);
            this.gridControlGenêrico.TabIndex = 16;
            this.gridControlGenêrico.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewGenêrico});
            this.gridControlGenêrico.Visible = false;
            // 
            // gridViewGenêrico
            // 
            this.gridViewGenêrico.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(221)))), ((int)(((byte)(245)))));
            this.gridViewGenêrico.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewGenêrico.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewGenêrico.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewGenêrico.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colCodigo3,
            this.colDataVenda3,
            this.colCliente3,
            this.colDescricao4,
            this.colCst2,
            this.colNomeVendedor2,
            this.colNcm2,
            this.colUnidade4,
            this.colDescricaoPlano4,
            this.colQuantidadeVendida4,
            this.colPrecoVenda4,
            this.colTipoSaida4,
            this.colCfop2,
            this.colBaseCalculo1,
            this.colTotalLiquido2,
            this.colTotalBruto1,
            this.colTotalDesconto2,
            this.colIcmsCredito,
            this.colPrecoBruto3,
            this.colReferencia1,
            this.colMarca1,
            this.colGrupo1,
            this.colRazaoSocial,
            this.colOperacao2,
            this.colValorBaseIcms1,
            this.colValorAliquota1});
            this.gridViewGenêrico.GridControl = this.gridControlGenêrico;
            this.gridViewGenêrico.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", this.colTotalLiquido2, "{0:n}")});
            this.gridViewGenêrico.Name = "gridViewGenêrico";
            this.gridViewGenêrico.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewGenêrico.OptionsView.ShowFooter = true;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            // 
            // colCodigo3
            // 
            this.colCodigo3.FieldName = "Codigo";
            this.colCodigo3.Name = "colCodigo3";
            this.colCodigo3.Visible = true;
            this.colCodigo3.VisibleIndex = 1;
            // 
            // colDataVenda3
            // 
            this.colDataVenda3.FieldName = "DataVenda";
            this.colDataVenda3.Name = "colDataVenda3";
            this.colDataVenda3.Visible = true;
            this.colDataVenda3.VisibleIndex = 2;
            // 
            // colCliente3
            // 
            this.colCliente3.FieldName = "Cliente";
            this.colCliente3.Name = "colCliente3";
            this.colCliente3.Visible = true;
            this.colCliente3.VisibleIndex = 3;
            // 
            // colDescricao4
            // 
            this.colDescricao4.FieldName = "Descricao";
            this.colDescricao4.Name = "colDescricao4";
            this.colDescricao4.Visible = true;
            this.colDescricao4.VisibleIndex = 4;
            // 
            // colCst2
            // 
            this.colCst2.FieldName = "Cst";
            this.colCst2.Name = "colCst2";
            this.colCst2.Visible = true;
            this.colCst2.VisibleIndex = 5;
            // 
            // colNomeVendedor2
            // 
            this.colNomeVendedor2.FieldName = "NomeVendedor";
            this.colNomeVendedor2.Name = "colNomeVendedor2";
            this.colNomeVendedor2.Visible = true;
            this.colNomeVendedor2.VisibleIndex = 6;
            // 
            // colNcm2
            // 
            this.colNcm2.FieldName = "Ncm";
            this.colNcm2.Name = "colNcm2";
            this.colNcm2.Visible = true;
            this.colNcm2.VisibleIndex = 7;
            // 
            // colUnidade4
            // 
            this.colUnidade4.FieldName = "Unidade";
            this.colUnidade4.Name = "colUnidade4";
            this.colUnidade4.Visible = true;
            this.colUnidade4.VisibleIndex = 8;
            // 
            // colDescricaoPlano4
            // 
            this.colDescricaoPlano4.FieldName = "DescricaoPlano";
            this.colDescricaoPlano4.Name = "colDescricaoPlano4";
            this.colDescricaoPlano4.Visible = true;
            this.colDescricaoPlano4.VisibleIndex = 9;
            // 
            // colQuantidadeVendida4
            // 
            this.colQuantidadeVendida4.FieldName = "QuantidadeVendida";
            this.colQuantidadeVendida4.Name = "colQuantidadeVendida4";
            this.colQuantidadeVendida4.Visible = true;
            this.colQuantidadeVendida4.VisibleIndex = 10;
            // 
            // colPrecoVenda4
            // 
            this.colPrecoVenda4.FieldName = "PrecoVenda";
            this.colPrecoVenda4.Name = "colPrecoVenda4";
            this.colPrecoVenda4.Visible = true;
            this.colPrecoVenda4.VisibleIndex = 11;
            // 
            // colTipoSaida4
            // 
            this.colTipoSaida4.FieldName = "TipoSaida";
            this.colTipoSaida4.Name = "colTipoSaida4";
            this.colTipoSaida4.Visible = true;
            this.colTipoSaida4.VisibleIndex = 12;
            // 
            // colCfop2
            // 
            this.colCfop2.FieldName = "Cfop";
            this.colCfop2.Name = "colCfop2";
            this.colCfop2.Visible = true;
            this.colCfop2.VisibleIndex = 13;
            // 
            // colBaseCalculo1
            // 
            this.colBaseCalculo1.FieldName = "BaseCalculo";
            this.colBaseCalculo1.Name = "colBaseCalculo1";
            this.colBaseCalculo1.Visible = true;
            this.colBaseCalculo1.VisibleIndex = 14;
            // 
            // colTotalLiquido2
            // 
            this.colTotalLiquido2.FieldName = "TotalLiquido";
            this.colTotalLiquido2.GroupFormat.FormatString = "{0:n}";
            this.colTotalLiquido2.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalLiquido2.Name = "colTotalLiquido2";
            this.colTotalLiquido2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalLiquido", "{0:n}")});
            this.colTotalLiquido2.Visible = true;
            this.colTotalLiquido2.VisibleIndex = 15;
            // 
            // colTotalBruto1
            // 
            this.colTotalBruto1.FieldName = "TotalBruto";
            this.colTotalBruto1.Name = "colTotalBruto1";
            this.colTotalBruto1.Visible = true;
            this.colTotalBruto1.VisibleIndex = 16;
            // 
            // colTotalDesconto2
            // 
            this.colTotalDesconto2.FieldName = "TotalDesconto";
            this.colTotalDesconto2.Name = "colTotalDesconto2";
            this.colTotalDesconto2.Visible = true;
            this.colTotalDesconto2.VisibleIndex = 17;
            // 
            // colIcmsCredito
            // 
            this.colIcmsCredito.FieldName = "IcmsCredito";
            this.colIcmsCredito.Name = "colIcmsCredito";
            this.colIcmsCredito.Visible = true;
            this.colIcmsCredito.VisibleIndex = 18;
            // 
            // colPrecoBruto3
            // 
            this.colPrecoBruto3.FieldName = "PrecoBruto";
            this.colPrecoBruto3.Name = "colPrecoBruto3";
            this.colPrecoBruto3.Visible = true;
            this.colPrecoBruto3.VisibleIndex = 19;
            // 
            // colReferencia1
            // 
            this.colReferencia1.FieldName = "Referencia";
            this.colReferencia1.Name = "colReferencia1";
            this.colReferencia1.Visible = true;
            this.colReferencia1.VisibleIndex = 20;
            // 
            // colMarca1
            // 
            this.colMarca1.FieldName = "Marca";
            this.colMarca1.Name = "colMarca1";
            this.colMarca1.Visible = true;
            this.colMarca1.VisibleIndex = 21;
            // 
            // colGrupo1
            // 
            this.colGrupo1.FieldName = "Grupo";
            this.colGrupo1.Name = "colGrupo1";
            this.colGrupo1.Visible = true;
            this.colGrupo1.VisibleIndex = 22;
            // 
            // colRazaoSocial
            // 
            this.colRazaoSocial.FieldName = "RazaoSocial";
            this.colRazaoSocial.Name = "colRazaoSocial";
            this.colRazaoSocial.Visible = true;
            this.colRazaoSocial.VisibleIndex = 23;
            // 
            // colOperacao2
            // 
            this.colOperacao2.FieldName = "Operacao";
            this.colOperacao2.Name = "colOperacao2";
            this.colOperacao2.Visible = true;
            this.colOperacao2.VisibleIndex = 24;
            // 
            // colValorBaseIcms1
            // 
            this.colValorBaseIcms1.FieldName = "ValorBaseIcms";
            this.colValorBaseIcms1.Name = "colValorBaseIcms1";
            this.colValorBaseIcms1.OptionsColumn.ReadOnly = true;
            this.colValorBaseIcms1.Visible = true;
            this.colValorBaseIcms1.VisibleIndex = 25;
            // 
            // colValorAliquota1
            // 
            this.colValorAliquota1.FieldName = "ValorAliquota";
            this.colValorAliquota1.Name = "colValorAliquota1";
            this.colValorAliquota1.OptionsColumn.ReadOnly = true;
            this.colValorAliquota1.Visible = true;
            this.colValorAliquota1.VisibleIndex = 26;
            // 
            // FrmMovimentacaoProdutos
            // 
            this.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 570);
            this.Controls.Add(this.gridControlPadrao2);
            this.Controls.Add(this.gridControlGenêrico);
            this.Controls.Add(this.gridControlPadrao1);
            this.Controls.Add(this.gridControlPostos);
            this.Controls.Add(this.gridControlMedicamentos);
            this.Controls.Add(this.gridControlPadrao3);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel15);
            this.LookAndFeel.SkinName = "The Bezier";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "FrmMovimentacaoProdutos";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pRODUTOSVENDIDOSPORPLANOBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mEDICAMENTOSVENDIDOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMBUSTIVEISVENDIDOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPostos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPostos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMedicamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMedicamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPadrao2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPadrao2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGenêrico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGenêrico)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel2;
        private Label label1;
        private DateTimePicker dataInicial;
        private DateTimePicker dataFinal;
        private Label label2;
        private Button btnBuscar;
        private Panel panel1;
        private Panel panel3;
        private Button simpleButton1;
        private DevExpress.Xpo.XPPageSelector xpPageSelector1;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida;
        private DevExpress.XtraGrid.Columns.GridColumn colValorTotal;
        private View.Exportacao exportacao1;
        private BindingSource pRODUTOSVENDIDOSPORPLANOBindingSource;
        private Button button2;
        private DevExpress.Data.Linq.EntityInstantFeedbackSource entityInstantFeedbackSource1;
        private BindingSource mEDICAMENTOSVENDIDOSBindingSource;
        private BindingSource cOMBUSTIVEISVENDIDOSBindingSource;
        private Button btnCriarBusca;
        private Panel panel4;
        private Panel panel7;
        private Label label3;
        private ComboBox comboBox1;
        private Button btnEditar;
        private Panel panel8;
        private Button btnExcluir;
        private Panel panel9;
        private Panel panel10;
        private Panel panel11;
        private DevExpress.XtraGrid.GridControl gridControlPostos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPostos;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEmissao;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidade;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano1;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor4;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescontoSubtotal;
        private DevExpress.XtraGrid.Columns.GridColumn colBico;
        private DevExpress.XtraGrid.Columns.GridColumn colBomba;
        private DevExpress.XtraGrid.Columns.GridColumn colTanque;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoTurno;
        private DevExpress.XtraGrid.Columns.GridColumn colVeiculoPlaca;
        private DevExpress.XtraGrid.Columns.GridColumn colVeiculoHodometro;
        private DevExpress.XtraGrid.Columns.GridColumn colVinculada;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVinculamento;
        private DevExpress.XtraGrid.Columns.GridColumn colNumero;
        private DevExpress.XtraGrid.Columns.GridColumn colHora;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPadrao3;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo6;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda5;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente6;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao7;
        private DevExpress.XtraGrid.Columns.GridColumn colCst4;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor6;
        private DevExpress.XtraGrid.Columns.GridColumn colNcm4;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade6;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano7;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida6;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda7;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida6;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop5;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseCalculo;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalLiquido5;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalBruto4;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDesconto4;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto5;
        private DevExpress.XtraGrid.Columns.GridColumn colReferencia5;
        private DevExpress.XtraGrid.Columns.GridColumn colMarca5;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo5;
        private DevExpress.XtraGrid.Columns.GridColumn colOperacao4;
        private DevExpress.XtraGrid.Columns.GridColumn colRazaoSocial4;
        private DevExpress.XtraGrid.Columns.GridColumn colValorBaseIcms4;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAliquota4;
        private DevExpress.XtraGrid.GridControl gridControlPadrao3;
        private DevExpress.XtraGrid.GridControl gridControlMedicamentos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMedicamentos;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda1;
        private DevExpress.XtraGrid.Columns.GridColumn colNotaFiscal;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao6;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop4;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida2;
        private DevExpress.XtraGrid.Columns.GridColumn colLote3;
        private DevExpress.XtraGrid.Columns.GridColumn colFabricacao3;
        private DevExpress.XtraGrid.Columns.GridColumn colVencimento3;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda6;
        private DevExpress.XtraGrid.Columns.GridColumn colDesconto;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano6;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente5;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor5;
        private DevExpress.XtraGrid.Columns.GridColumn colMarca4;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo4;
        private DevExpress.XtraGrid.Columns.GridColumn colRazaoSocial3;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida2;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoCusto;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalLiquido4;
        private DevExpress.XtraGrid.Columns.GridColumn colOperacao5;
        private DevExpress.XtraGrid.Columns.GridColumn colReferencia4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPadrao1;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo1;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao2;
        private DevExpress.XtraGrid.Columns.GridColumn colCst;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor;
        private DevExpress.XtraGrid.Columns.GridColumn colNcm;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano2;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda2;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida1;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalLiquido;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalBruto3;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDesconto1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto;
        private DevExpress.XtraGrid.Columns.GridColumn colReferencia3;
        private DevExpress.XtraGrid.Columns.GridColumn colMarca3;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo3;
        private DevExpress.XtraGrid.Columns.GridColumn colOperacao;
        private DevExpress.XtraGrid.Columns.GridColumn colRazaoSocial1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorBaseIcms3;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAliquota3;
        private DevExpress.XtraGrid.GridControl gridControlPadrao1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPadrao2;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo2;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda2;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao3;
        private DevExpress.XtraGrid.Columns.GridColumn colCst1;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor1;
        private DevExpress.XtraGrid.Columns.GridColumn colNcm1;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano3;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida3;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda3;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida3;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalLiquido1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalBruto;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDesconto;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto2;
        private DevExpress.XtraGrid.Columns.GridColumn colReferencia;
        private DevExpress.XtraGrid.Columns.GridColumn colMarca;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo;
        private DevExpress.XtraGrid.Columns.GridColumn colOperacao1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorBaseIcms;
        private DevExpress.XtraGrid.Columns.GridColumn colRazaoSocial2;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAliquota;
        private DevExpress.XtraGrid.GridControl gridControlPadrao2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private Panel panel12;
        private Panel panel13;
        private Panel panel14;
        private Panel panel15;
        private DevExpress.XtraGrid.GridControl gridControlGenêrico;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGenêrico;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo3;
        private DevExpress.XtraGrid.Columns.GridColumn colDataVenda3;
        private DevExpress.XtraGrid.Columns.GridColumn colCliente3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricao4;
        private DevExpress.XtraGrid.Columns.GridColumn colCst2;
        private DevExpress.XtraGrid.Columns.GridColumn colNomeVendedor2;
        private DevExpress.XtraGrid.Columns.GridColumn colNcm2;
        private DevExpress.XtraGrid.Columns.GridColumn colUnidade4;
        private DevExpress.XtraGrid.Columns.GridColumn colDescricaoPlano4;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantidadeVendida4;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoVenda4;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoSaida4;
        private DevExpress.XtraGrid.Columns.GridColumn colCfop2;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseCalculo1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalLiquido2;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalBruto1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalDesconto2;
        private DevExpress.XtraGrid.Columns.GridColumn colIcmsCredito;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecoBruto3;
        private DevExpress.XtraGrid.Columns.GridColumn colReferencia1;
        private DevExpress.XtraGrid.Columns.GridColumn colMarca1;
        private DevExpress.XtraGrid.Columns.GridColumn colGrupo1;
        private DevExpress.XtraGrid.Columns.GridColumn colRazaoSocial;
        private DevExpress.XtraGrid.Columns.GridColumn colOperacao2;
        private DevExpress.XtraGrid.Columns.GridColumn colValorBaseIcms1;
        private DevExpress.XtraGrid.Columns.GridColumn colValorAliquota1;
    }
}