﻿
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using MegaReporter.Models.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MegaReporter.View
{
    public partial class Exportacao : UserControl
    {
        public DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink1;
        private DevExpress.XtraGrid.GridControl gridControlComponent;

        public bool Landscape = true;
        public string TituloDocumento;
        public string SubTitulo;
        public string NomeFantasia;
        public string Slogan;
        public string Cnpj;
        public bool AjustaTamanhoColunas = true;

        public event CreateAreaEventHandler OnImprimeCabecalho;
        public event EventHandler OnAntesImprimir;

        public string NomeArquivo
        {
            get
            {
                return TituloDocumento
                    .Replace(" ", "_")
                    .Replace("/", ".")
                    .Replace("(", "")
                    .Replace(")", "")
                    .Replace(":", "")
                    .Replace("/", "")
                    .Replace("\\", "")
                    .Replace("*", "")
                    .Replace("?", "")
                    .Replace("\"", "")
                    .Replace("<", "")
                    .Replace(">", "")
                    .Replace("|", "")
                    ;
            }
        }


        public Exportacao()
        {
            InitializeComponent();

            itemCSV.ItemClick += MnuCSV_Click;
            itemDOCX.ItemClick += MnuDOCX_Click;
            itemHTML.ItemClick += MnuHTML_Click;
            itemMHT.ItemClick += MnuMHT_Click;
            itemPDF.ItemClick += MnuPDF_Click;
            itemRTF.ItemClick += MnuRTF_Click;
            itemTXT.ItemClick += MnuTEXT_Click;
            itemXLS.ItemClick += MnuXLS_Click;
            itemXLSX.ItemClick += MnuXLSX_Click;

            mnuCSV.Click += MnuCSV_Click;
            mnuDOCX.Click += MnuDOCX_Click;
            mnuHTML.Click += MnuHTML_Click;
            mnuMHT.Click += MnuMHT_Click;
            mnuPDF.Click += MnuPDF_Click;
            mnuRTF.Click += MnuRTF_Click;
            mnuTEXT.Click += MnuTEXT_Click;
            mnuXLS.Click += MnuXLS_Click;
            mnuXLSX.Click += MnuXLSX_Click;


            btnVisualizarImpressao.Click += BtnVisualizarImpressao_Click;
            // btnImprimir.Click += BtnImprimir_Click;


            DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();
            printableComponentLink1 = new DevExpress.XtraPrinting.PrintableComponentLink();
            ps.Links.AddRange(new object[] { printableComponentLink1 });

            printableComponentLink1.PrintingSystem = ps;
            printableComponentLink1.PrintingSystemBase = ps;
            printableComponentLink1.CreateReportHeaderArea += link_CreateReportHeaderArea;           
        }
        
        public DevExpress.XtraGrid.GridControl GridControlComponent
        {
            get
            {
                return gridControlComponent;
            }
            set
            {
                gridControlComponent = value;
                gridControlComponent.MainView.PrintInitialize += gridView1_PrintInitialize;
                printableComponentLink1.Component = value;
                if (gridControlComponent.MainView is GridView)
                    (gridControlComponent.MainView as GridView).OptionsPrint.AutoWidth = false;

            }
        }
        


        private void gridView1_PrintInitialize(object sender, DevExpress.XtraGrid.Views.Base.PrintInitializeEventArgs e)
        {
           
  
            
            if (this.AjustaTamanhoColunas)
                if (gridControlComponent.MainView is GridView)
                    (gridControlComponent.MainView as GridView).BestFitColumns();


            PrintingSystemBase pb = e.PrintingSystem as PrintingSystemBase;
            pb.PageSettings.Landscape = Landscape;
            
            pb.PageSettings.LeftMargin = 1;
            pb.PageSettings.RightMargin = 1;
            pb.PageSettings.TopMargin = 46;
            pb.PageSettings.BottomMargin = 1;

            

            string rightColumn = "\n\nPágina: " + DevExpress.XtraPrinting.Localization.PreviewLocalizer.GetString(DevExpress.XtraPrinting.Localization.PreviewStringId.PageInfo_PageNumberOfTotal);
            string middleColumn = "\n\nOperador: " + "Lee Ruang";
            string leftColumn = "\n\nMegaFlex Gestão 1.0.0  =>  Data: " + DevExpress.XtraPrinting.Localization.PreviewLocalizer.GetString(DevExpress.XtraPrinting.Localization.PreviewStringId.PageInfo_PageDate); 
            // Create a PageHeaderFooter object and initializing it with  
            // the link's PageHeaderFooter.  
            
            PageHeaderFooter phf = printableComponentLink1.PageHeaderFooter as PageHeaderFooter;
            // Add custom information to the link's header.  

            phf.Header.Content.Clear();


            phf.Header.Content.AddRange(new string[] { leftColumn, middleColumn, rightColumn });

            phf.Header.LineAlignment = BrickAlignment.Near;
            phf.Header.Font = new Font("Arial", 09, FontStyle.Regular);


        }


        private void AbrirArquivoSalvo()
        {
            Process.Start(new ProcessStartInfo { FileName = saveFileDialog.FileName, UseShellExecute = true });
        }
        private void BtnImprimir_Click(object sender, EventArgs e)
        {

            OnAntesImprimir?.Invoke(sender, e);

            printableComponentLink1.CreateDocument();
            printableComponentLink1.Print();
        }

        private void BtnVisualizarImpressao_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

 
            OnAntesImprimir?.Invoke(sender, e);
            printableComponentLink1.CreateDocument();
            printableComponentLink1.ShowPreviewDialog();
            //UserLookAndFeel lookAndFeel = new UserLookAndFeel(ownerControl: 0);
            //printableComponentLink1.ShowRibbonPreviewDialog(lookAndFeel);
            Cursor.Current = Cursors.Default;
        }



        private void link_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
           

            int y = 0;

  

            string reportHeaderEmpresa = "Empresa teste";
                    e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Near);
                    e.Graph.Font = new Font("Arial", 09, FontStyle.Regular);
                    e.Graph.Font = new Font("Arial", 09, FontStyle.Regular);
                    RectangleF rec1 = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);


            e.Graph.DrawString(NomeFantasia + "  =>  CNPJ: " + Cnpj , Color.Black, rec1, BorderSide.None);
                    y += 15;
                    e.Graph.Font = new Font("Arial", 09, FontStyle.Regular);
                    rec1 = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);
                    e.Graph.DrawString(Slogan, Color.Black, rec1, BorderSide.None);

            y += 15;
                   

            if (TituloDocumento != "")
            {
                string reportHeader = TituloDocumento;
                e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
                e.Graph.Font = new Font("Arial", 12, FontStyle.Bold);
                RectangleF rec = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);
                e.Graph.DrawString(reportHeader, Color.Black, rec, BorderSide.None);

            }
            if (SubTitulo != "")
            {
                y = y + 22;
                string reportSubtituloHeader = SubTitulo;
                e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
                e.Graph.Font = new Font("Arial", 10, FontStyle.Regular);
                RectangleF rec = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);
                e.Graph.DrawString(reportSubtituloHeader, Color.Black, rec, BorderSide.None);
            }


            
            if (gridControlComponent.MainView is GridView)
            {
                if ((gridControlComponent.MainView as GridView).GroupedColumns.Count > 0)
                {
                    string textoAgrupamentos = "";

                    //imprime os agrupamentos
                    foreach (var grupo in (gridControlComponent.MainView as GridView).GroupedColumns)
                    {
                        textoAgrupamentos += grupo.ToString() + ",";
                    }

                    if (textoAgrupamentos.EndsWith(","))
                    {
                        textoAgrupamentos = textoAgrupamentos.Remove(textoAgrupamentos.Length - 1);
                    }

                    if (textoAgrupamentos.Trim() != "")
                    {

                        y = y + 15;
                        string reportSubtituloHeader = "Agrupado por: " + textoAgrupamentos;
                        e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
                        e.Graph.Font = new Font("Arial", 10, FontStyle.Regular);
                        RectangleF rec = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);
                        e.Graph.DrawString(reportSubtituloHeader, Color.Black, rec, BorderSide.None);
                    }
                }
               
            }
            



            if (gridControlComponent.MainView is GridView)
            {
                if ((gridControlComponent.MainView as GridView).GetFilterDisplayText((gridControlComponent.MainView as GridView).ActiveFilterCriteria) != "")
                {
                    string filtro = "Filtro: " + ((gridControlComponent.MainView as GridView).GetFilterDisplayText((gridControlComponent.MainView as GridView).ActiveFilterCriteria));

                    y = y + 15;
                    string reportSubtituloHeader = filtro;
                    e.Graph.StringFormat = new BrickStringFormat(StringAlignment.Center);
                    e.Graph.Font = new Font("Tahoma", 10, FontStyle.Regular);
                    RectangleF rec = new RectangleF(0, y, e.Graph.ClientPageSize.Width, 50);
                    e.Graph.DrawString(reportSubtituloHeader, Color.Black, rec, BorderSide.None);
                }
            }






            OnImprimeCabecalho?.Invoke(sender, e);
            
        }


        private static Image ResizeImage(Image img, RectangleF rect)
        {
            try
            {
                //image must fit into our rectangle.  Determine resizing factor base on width first  

                double shrinkw = (double)rect.Width / img.Width;
                double shrinkh = (double)rect.Height / img.Height;

                //if the resulting image is taller than height of rectangle, set the shrinkage factor lower  
                double shrinkby = Math.Min(shrinkw, shrinkh);

                var newImageWidth = (int)(img.Width * shrinkby);
                var newImageHeight = (int)(img.Height * shrinkby);

                //proportionally resize the image to fit the rectangle.  
                Bitmap newImg = new Bitmap(newImageWidth, newImageHeight);
                Graphics graphic = Graphics.FromImage(newImg);
                graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;

                //draw the newly resized image  
                graphic.DrawImage(img, 0, 0, newImageWidth, newImageHeight);
                //dispose and free up the resources  
                graphic.Dispose();
                return newImg;
            }
            catch
            {
                return null;
            }
        }



        private void MnuXLSX_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .xlsx";
            saveFileDialog.FileName = NomeArquivo + ".xlsx";
            saveFileDialog.Filter = "Documento do Excel| *.xlsx";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToXlsx(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }


        private void MnuXLS_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .xls";
            saveFileDialog.FileName = NomeArquivo + ".xls";
            saveFileDialog.Filter = "Documento do Excel| *.xls";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToXls(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuTEXT_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .txt";
            saveFileDialog.FileName = NomeArquivo + ".txt";
            saveFileDialog.Filter = "Documento de Texto| *.txt";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {

                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToText(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuRTF_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .rtf";
            saveFileDialog.FileName = NomeArquivo + ".rtf";
            saveFileDialog.Filter = "Documento RTF| *.rtf";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToRtf(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuPDF_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .pdf";
            saveFileDialog.FileName = NomeArquivo + ".pdf";
            saveFileDialog.Filter = "Documento PDF| *.pdf";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToPdf(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuMHT_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .mht";
            saveFileDialog.FileName = NomeArquivo + ".mht";
            saveFileDialog.Filter = "Documento MHT| *.mht";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToMht(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuHTML_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .html";
            saveFileDialog.FileName = NomeArquivo + ".html";
            saveFileDialog.Filter = "Documento Html| *.html";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToHtml(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuDOCX_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .docx";
            saveFileDialog.FileName = NomeArquivo + ".docx";
            saveFileDialog.Filter = "Documento do Word| *.docx";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToDocx(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MnuCSV_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Salvar arquivo .csv";
            saveFileDialog.FileName = NomeArquivo + ".csv";
            saveFileDialog.Filter = "Documento CSV| *.csv";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                printableComponentLink1.CreateDocument();
                printableComponentLink1.ExportToCsv(saveFileDialog.FileName);
                AbrirArquivoSalvo();
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnVisualizarImpressao_Click_1(object sender, EventArgs e)
        {

        }
    }


}


