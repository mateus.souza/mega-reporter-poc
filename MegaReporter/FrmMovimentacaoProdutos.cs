﻿using DevExpress.CodeParser;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.ReportGeneration;
using DevExpress.XtraReports.UI;
using MegaReporter.Models;
using MegaReporter.Models.Controllers;
using MegaReporter.Models.Entities;
using MegaReporter.Models.Entities.Loja;
using MegaReporter.Models.Repositories;
using MegaReporter.View;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;

namespace MegaReporter
{
    public partial class FrmMovimentacaoProdutos : FrmBase
    {

        private readonly MovimentacaoProdutosController _produtosVendidosPorPlanoController;
        private readonly ConfiguracoesController _configuracoesController;
        private readonly Exportacao Exportacao;
        private readonly DevExpressSettings devExpressSettings;


        void Summary_CollectionChanged(object sender, CollectionChangeEventArgs e)
        {
            GridColumn gc = gridViewPadrao1.Columns.ColumnByFieldName((e.Element as GridSummaryItem).FieldName);
        }

        public FrmMovimentacaoProdutos(MovimentacaoProdutosController produtosVendidosPorPlanoController, 
            ConfiguracoesController configuracoesController)
        {
            InitializeComponent();


            
            _produtosVendidosPorPlanoController = produtosVendidosPorPlanoController;
            devExpressSettings = new DevExpressSettings();

            Exportacao = new Exportacao();
            panel3.Controls.Add(Exportacao);

                
            devExpressSettings.CaminhaDataParaDireita(dataInicial, dataFinal);

            
            _configuracoesController = configuracoesController;

            var dadosLoja = _configuracoesController.GetDadosLoja();

            Exportacao.Slogan = dadosLoja.Slogan;
            Exportacao.NomeFantasia = dadosLoja.NomeFantasia;
            Exportacao.Cnpj = dadosLoja.Cnpj;

            Exportacao.AjustaTamanhoColunas = true;

            gridControlPadrao1.Visible = true;
            gridControlPadrao1.Dock = DockStyle.Fill;
            gridViewPadrao1.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 1 - Todas As Vendas.xml");
            //tabControl1.SelectedIndexChanged += new EventHandler(TabControl_SelectedIndexChanged);


            //gridView2.ShowCustomization();

            gridViewPadrao1.Columns["Operacao"].OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            gridViewPadrao3.Columns["Operacao"].OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            gridViewPadrao2.Columns["Operacao"].OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;

            panel1.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel2.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel3.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel4.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel7.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel8.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel9.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel10.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel11.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel12.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel13.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel14.BackColor = ColorTranslator.FromHtml("#F6F6F6");
            panel15.BackColor = ColorTranslator.FromHtml("#F6F6F6");

            btnCriarBusca.BackColor = ColorTranslator.FromHtml("#2F01B2");
            btnCriarBusca.ForeColor = ColorTranslator.FromHtml("#FFFFFF");

            btnBuscar.BackColor = ColorTranslator.FromHtml("#2F01B2");
            btnBuscar.ForeColor = ColorTranslator.FromHtml("#FFFFFF");

            btnEditar.BackColor = ColorTranslator.FromHtml("#2F01B2");
            btnEditar.ForeColor = ColorTranslator.FromHtml("#FFFFFF");

            btnExcluir.BackColor = ColorTranslator.FromHtml("#2F01B2");
            btnExcluir.ForeColor = ColorTranslator.FromHtml("#FFFFFF");




            InitCombobox();
            comboBox1.SelectedIndex = 0;
            DesabilitaOpçõesComboBox();

            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        //Busca Layouts Customizados do cliente
        public void InitCombobox()
        {
            string[] files = Directory.GetFiles("./Layouts/Customizados/");
            var namesOnly = files.Select(f => Path.GetFileNameWithoutExtension(f)).ToArray();
            comboBox1.Items.AddRange(namesOnly);
            comboBox1.Items.Remove(namesOnly);
        }
        
        private void DesabilitaOpçõesComboBox()
        {    
            if(_configuracoesController.ComercializaCombustivel() is false)
            {
                comboBox1.Items.Remove("Padrão 5 - Posto de Combustíveis");
            }
            if (_configuracoesController.ComercializaMedicamento() is false)
            {
                comboBox1.Items.Remove("Padrão 4 - Medicamentos");
            }
            
        }
        

     
        private void lblDataInicial_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Pesquisar();
        }

        private void Pesquisar()
        {            
            if (comboBox1.SelectedIndex == 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodos(dataInicial.Text, dataFinal.Text);
                pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                   $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewPadrao1, gridControlPadrao1, "Padrao1-TodasAsVendas.xml");

                Cursor.Current = Cursors.Default;
            }
            else if(comboBox1.SelectedIndex == 1)
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodos(dataInicial.Text, dataFinal.Text);
                pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                   $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewPadrao2, gridControlPadrao2, "GroupVendasPorDia.xml");

                Cursor.Current = Cursors.Default;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodos(dataInicial.Text, dataFinal.Text);
                pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                    $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewPadrao3, gridControlPadrao3, "Padrao2-FinsContabeis.xml");

                Cursor.Current = Cursors.Default;
            }
            else if(comboBox1.SelectedItem == "Padrão 4 - Medicamentos")
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodosMedicamentos(dataInicial.Text, dataFinal.Text);
                mEDICAMENTOSVENDIDOSBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                    $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewMedicamentos, gridControlMedicamentos, "Padrao2-FinsContabeis.xml");

                Cursor.Current = Cursors.Default;
            }
            
            else if (comboBox1.SelectedItem == "Padrão 5 - Posto de Combustíveis")
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodosCombustiveis(dataInicial.Text, dataFinal.Text);
                cOMBUSTIVEISVENDIDOSBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                    $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewPostos, gridControlPostos, "abastecimentos.xml");

                Cursor.Current = Cursors.Default;
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;

                var consulta = _produtosVendidosPorPlanoController.ExibeTodos(dataInicial.Text, dataFinal.Text);
                pRODUTOSVENDIDOSPORPLANOBindingSource.DataSource = consulta;

                devExpressSettings.ConfiguraImpressaoeGrid(Exportacao, "Relatório Geral de Vendas por Período",
                    $"Período: {dataInicial.Text} a {dataFinal.Text}", gridViewGenêrico, gridControlGenêrico, "abastecimentos.xml");

                Cursor.Current = Cursors.Default;
            }
            
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void exportacao1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            gridViewPadrao2.SaveLayoutToXml("louco.xml");
        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                btnEditar.Visible = false;
                panel8.Visible = false;

                btnExcluir.Visible = false;
                panel9.Visible = false;
                panel11.Visible = true;

                gridControlPadrao1.Visible = true;
                gridControlPadrao1.Dock = DockStyle.Fill;
                gridControlPadrao2.Visible = false;
                gridControlPadrao3.Visible = false;
                gridControlMedicamentos.Visible = false;
                gridControlPostos.Visible = false;

              
                gridViewPadrao1.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 1 - Todas as Vendas.xml");


                gridControlGenêrico.Visible = false;

            }
            else if (comboBox1.SelectedIndex == 1)
            {
                btnEditar.Visible = false;
                panel8.Visible = false;

                btnExcluir.Visible = false;
                panel9.Visible = false;
                panel11.Visible = true;

                gridControlPadrao2.Visible = true;
                gridControlPadrao2.Dock = DockStyle.Fill;
                gridControlPadrao1.Visible = false;
                gridControlPadrao3.Visible = false;
                gridControlMedicamentos.Visible = false;
                gridControlPostos.Visible = false;

                gridControlGenêrico.Visible = false;

                gridViewPadrao2.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 2 - Por Plano de Pagamento.xml");

            }
            else if (comboBox1.SelectedIndex == 2)
            {
                
                btnEditar.Visible = false;
                panel8.Visible = false;

                btnExcluir.Visible = false;
                panel9.Visible = false;
                panel11.Visible = true;

                gridControlPadrao3.Visible = true;
                gridControlPadrao3.Dock = DockStyle.Fill;
                gridControlPadrao1.Visible = false;
                gridControlPadrao2.Visible = false;
                gridControlMedicamentos.Visible = false;
                gridControlPostos.Visible = false;

                gridControlGenêrico.Visible = false;


                gridViewPadrao3.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 3 - Fins Contábeis.xml");

            }
            else if (comboBox1.SelectedItem == "Padrão 4 - Medicamentos")
            {

                btnEditar.Visible = false;
                panel8.Visible = false;

                btnExcluir.Visible = false;
                panel9.Visible = false;
                panel11.Visible = true;

                gridControlGenêrico.Visible = false;
               
                gridControlPadrao1.Visible = false;
                gridControlPadrao2.Visible = false;
                gridControlPadrao3.Visible = false;
                
                gridControlPostos.Visible = false;
                gridControlGenêrico.Visible = false;


                gridControlMedicamentos.Visible = true;
                gridControlMedicamentos.Dock = DockStyle.Fill;
               

                gridViewMedicamentos.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 4 - Medicamentos.xml");

                

            }
            else if (comboBox1.SelectedItem == "Padrão 5 - Posto de Combustíveis")
            {

                btnEditar.Visible = false;
                panel8.Visible = false;

                btnExcluir.Visible = false;
                panel9.Visible = false;
                panel11.Visible = true;

                gridControlGenêrico.Visible = false;
                gridControlPostos.Dock = DockStyle.Fill;
                gridControlPadrao1.Visible = false;
                gridControlPadrao2.Visible = false;
                gridControlMedicamentos.Visible = false;
                gridControlPostos.Visible = true;

                gridControlGenêrico.Visible = false;

                gridViewPostos.RestoreLayoutFromXml("./Layouts/Padrões/Padrão 5 - Posto de Combustíveis.xml");
            }
            else if (comboBox1.SelectedItem == "Padrão 6 - Genêrico")
            {
                gridControlGenêrico.Visible = true;
                gridControlGenêrico.Dock = DockStyle.Fill;

                btnEditar.Visible = false;
                panel8.Visible = false;
                btnExcluir.Visible = false;
                panel9.Visible = false;
            }
            else
            {
                gridControlGenêrico.Visible = true;
                gridControlGenêrico.Dock = DockStyle.Fill;
                gridViewGenêrico.RestoreLayoutFromXml($"./Layouts/Customizados/{comboBox1.SelectedItem.ToString()}.xml");

                btnEditar.Visible = true;
                panel8.Visible = true;
                btnExcluir.Visible = true;
                panel9.Visible = true;               
            }
        }

        private void gridControl2_Click(object sender, EventArgs e)
        {

        }

        private void btnCriarBusca_Click(object sender, EventArgs e)
        {
            // Microsoft.VisualBasic.Interaction.InputBox("Your Message ", "Title", "Default Response");
            MessageBox.Show("Funciona, más não é bom usar!!! -- Layout povo louco criado com sucesso!");
            gridViewMedicamentos.SaveLayoutToXml("./Layouts/Customizados/OhPovoLouco.xml");
            InitCombobox();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja excluir o Layout?", "Exclusão de Layout", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string path = $"./Layouts/Customizados/{comboBox1.SelectedItem.ToString()}.xml";
                File.Delete(path);

                comboBox1.Items.Remove(comboBox1.SelectedItem);

                MessageBox.Show("Layout excluido com sucesso!");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja concluir a edição o Layout?", "Edição de Layout", MessageBoxButtons.YesNo,
               MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string path = $"./Layouts/Customizados/{comboBox1.SelectedItem.ToString()}.xml";

                gridViewGenêrico.SaveLayoutToXml(path);

                MessageBox.Show("Layout editado com sucesso!");
            }
        }
    }
}