using MegaReporter.Models;
using MegaReporter.Models.Controllers;
using MegaReporter.Models.Entities;
using MegaReporter.Models.Repositories;
using MegaReporter.Models.Repositories.Configuração;
using MegaReporter.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Globalization;

namespace MegaReporter
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Create a new object, representing the German culture. 
           

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var host = CreateHostBuilder().Build();
            ServiceProvider = host.Services;

            //ArgsExec argsExec = new ArgsExec();
            //if(argsExec.AbreFormulario() == "FrmMovimentacaoProdutos")
            //{
            
            Application.Run(ServiceProvider.GetRequiredService<FrmMovimentacaoProdutos>());
            //}


        }

        public static IServiceProvider ServiceProvider { get; private set; }
        static IHostBuilder CreateHostBuilder()
        {

            return Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) => {
                    services.AddTransient<FrmMovimentacaoProdutos>();
                    

                    services.AddDbContext<MegaReporterDbContext>(options =>
                    options.UseFirebird("User = SYSDBA; " +
"Password=masterkey;" +
$"Database={ConexaoBanco.StringConexao()};" +
"DataSource=localhost;" +
"Port=3050;" +
"charset=win1252;"));

                    services.AddTransient<FrmMovimentacaoProdutos>();
                    services.AddTransient<IProdutosMovimentadosDAO<PRODUTOS>, ProdutosMovimentadosDAO>();
                    services.AddTransient<MovimentacaoProdutosController>();
                    

                    services.AddTransient<IConfiguracaoRepository, ConfiguracaoRepository>();
                    services.AddTransient<ConfiguracoesController>();
                });
        }
    }
}